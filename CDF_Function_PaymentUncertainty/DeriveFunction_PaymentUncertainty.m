% Trying with Symbolic Toolbox (Too Slow)

% syms x g 
% 
% b = 2;
% x_point = 0.5;
% y_value = 0.5;
% 
% g = -log((1/y_value - 1)^(1/b)+1)/log(x_point)
% 
% figure
% % for b = [1/10,1/8,1/6,1/4,1/2,2,4,6,8,10]
% for b = [1/10,1/4]
%     y=1/(1+(x^g/(1-x^g))^-b);
%     fplot(y)
%     hold on
% end
% 
% xlim([0 1])
% ylim([0 1])

%%  Function Working 

discretization = 0.001;
range = 0:discretization:1;
x= range; % set grid space 


% b = 2;
x_point = 130/260;
y_value = 0.5;


figure
for b = 2
% for b = [1,2,5,2000] 
% for b = [1/10,1/4]
    g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
    
    y=@(x) 1/(1+(x^g/(1-x^g))^-b);
    
    fplot(y,[0 1])
    hold on
end

xlim([0 1])
ylim([0 1])

% b = 2;
% figure
% for x_point = [0.2,0.4,0.5,0.7]
% % for b = [1/10,1/4]
%     g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
%     y=@(x) 1/(1+(x^g/(1-x^g))^-b);
%     fplot(y,[0 1])
%     hold on
% end
% 
% xlim([0 1])
% ylim([0 1])


%% Trying Probability Stuff


l = 1;
for x = range
    te(l) = 1/(1+(x^g/(1-x^g))^-b);
    l=l+1;
end

% te = te+;

figure
plot(range,te)

por = diff(te);
figure
plot(range(2:end),por)

randomSamples = datasample(por,1000, 'Replace',false);

for l = 1:length(randomSamples)
    IndexOfRandSamples(1,l) = find(por==randomSamples(l));
end

finalScens = range(IndexOfRandSamples)*130*2*10^6;

probabilitiesOfFinalScens = randomSamples;