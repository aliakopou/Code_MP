% function y = visualizeCustomCDF(b,x_point,y_value)

% The goal of the function is to just create the fig and visualize the CDF

b = 3;
% x_point = 130/260;
x_point = 0.7;
y_value = 0.5;


figure
% for b = 2
% for b = [1,2,5,2000]
% for b = [1/10,1/4]
g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);

y=@(x) 1/(1+(x^g/(1-x^g))^-b);

fplot(y,[0 1])
% hold on
% end

xlim([0 1])
ylim([0 1])


%% Representing CDF of Gaussian using error function (erf)

% Set the mean (mu) and a very small standard deviation (sigma)
mu = 0.7;
sigma = 0.1;

% Define the Gaussian CDF function
gaussian_cdf = @(x) 0.5 * (1 + erf((x - mu) / (sigma * sqrt(2))));

% Create the x values for plotting
x_values = linspace(0, 1, 1000);

% Calculate the Gaussian CDF values
cdf_values = gaussian_cdf(x_values);

% Plot the Gaussian CDF
% figure;
hold on
plot(x_values, cdf_values, 'LineWidth', 2);
xlabel('x');
ylabel('CDF');
title(['Gaussian CDF with \mu = ', num2str(mu), ' and \sigma = ', num2str(sigma)]);
grid on;

