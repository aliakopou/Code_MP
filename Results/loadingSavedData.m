%% Log, realization 08 

%2 iterations
load('log_decay1_real08')
load('log_decay2_real08')
% load('log_decay3_real08')
% load('log_decay4_real08')
load('log_decay5_real08')

%% Log, realization 02

%2 iterations
load('log_decay1_real02')
load('log_decay2_real02')
load('log_decay5_real02')


%10 iterations
load('log_it10_decay1_real02')




%% Exp, realization 08 

%2 iterations
% load('exp_curve02_real08')
load('exp_curve05_real08')
load('exp_curve1_real08')
% load('exp_curve1point5_real08')
load('exp_curve2point5_real08')
load('exp_curve2point5_real06')


%% Exp, realization 02

%2 iterations
% load('exp_curve05_real02')
load('exp_curve05_real02_run2')
% load('exp_curve1_real02')
load('exp_curve1_real02_run2')
% load('exp_curve1point5_real02')
% load('exp_curve2point5_real02')
load('exp_curve2point5_real02_run2')

%3 iterations
load('exp_it3_curve05_real02')

%10 iterations
% load('exp_it10_curve02_real02')
load('exp_it10_curve05_real02')