clear all 
%%
load('log5_it200_02')

obj_it1 = obj_it;
crps_it1 = crps_it;

optPayment1 = optPayment.';
minn1 = minn;
%%
clearvars -except obj_it1 crps_it1 optPayment1 minn1

load('log5_it200_04')

obj_it2 = obj_it;
crps_it2 = crps_it;

optPayment2 = optPayment.';
minn2 = minn;
%%
clearvars -except obj_it1 crps_it1 optPayment1 minn1 obj_it2 crps_it2 optPayment2 minn2

load('log5_it200_06')

obj_it3 = obj_it;
crps_it3 = crps_it;

optPayment3 = optPayment.';
minn3 = minn;

%%
clearvars -except obj_it1 crps_it1 optPayment1 minn1 obj_it2 crps_it2 optPayment2 minn2 obj_it3 crps_it3 optPayment3 minn3 

load('log5_it200_08')

obj_it4 = obj_it;
crps_it4 = crps_it;

optPayment4 = optPayment.';
minn4 = minn;

%% Combine All

clearvars -except obj_it1 crps_it1 optPayment1 minn1 obj_it2 crps_it2 optPayment2 minn2 obj_it3 crps_it3 optPayment3 minn3 obj_it4 crps_it4 optPayment4 minn4

obj_it = cat(1,obj_it1,obj_it2,obj_it3,obj_it4);
crps_it = cat(1,crps_it1,crps_it2,crps_it3,crps_it4);
optPayment = cat(1,optPayment1,optPayment2,optPayment3,optPayment4);
minn = cat(1,minn1,minn2,minn3,minn4);

save('AllRealizations_log5')
