

%% NO
time_steps = 1:24; % Set up random data for demonstration purposes
point_forecast = rand(1, 24) * 10;
quantiles = [0.1, 0.3, 0.5, 0.7, 0.9];
probabilistic_forecast = quantile(rand(1000, 24), quantiles);

figure('Position', [100, 100, 900, 400]); % create the first figure for the point forecast
plot(time_steps, point_forecast, 'LineWidth', 2);
xlabel('Time (hours)');
ylabel('Point Forecast');
title('24-hour Point Forecast');
xlim([1, 24]);
xticks(1:1:24);
grid on;

figure('Position', [100, 100, 900, 400]); % create the second figure for the probabilistic forecast

colors = [0.8, 0.8, 1.0; % define colors for each quantile range (shades of blue)
          0.6, 0.6, 1.0];

for i = 1:(length(quantiles) - 1) / 2 % plot shaded areas for quantile ranges
    fill([time_steps, fliplr(time_steps)], [probabilistic_forecast(i, :), fliplr(probabilistic_forecast(end-i+1, :))], colors(i, :), 'LineStyle', 'none');
    hold on;
end

plot(time_steps, probabilistic_forecast(ceil(end/2), :), 'LineWidth', 2, 'Color', 'k'); % plot the median line
xlabel('Time (hours)');
ylabel('Probabilistic Forecast');
title('24-hour Probabilistic Forecast (Multiple Quantile Ranges)');
xlim([1, 24]);
xticks(1:1:24);
grid on;

lgd = legend('10%-90% Quantile Range', '30%-70% Quantile Range', '50% Quantile (Median)'); % define the legend
title(lgd, 'Quantiles');
hold off;


%% Obtaining PDF and CDF for timestep 2 of probabilistic forecast

data_timestep_2 = rand(1000, 1) * 10; % Set up random data for demonstration purposes

[PDF, X] = ksdensity(data_timestep_2); % Estimate the PDF using ksdensity function
CDF = cumsum(PDF) * (X(2) - X(1)); % Compute the CDF

figure('Position', [100, 100, 600, 600]); % Create the first figure for the PDF
plot(X, PDF, 'LineWidth', 2);
xlabel(''); % Remove x-axis label
ylabel('Probability Density');
title('PDF for Timestep 2');
grid on;
set(gca, 'box', 'off');

figure('Position', [100, 100, 600, 600]); % Create the second figure for the CDF
plot(X, CDF, 'LineWidth', 2);
xlabel(''); % Remove x-axis label
ylabel('Cumulative Probability');
title('CDF for Timestep 2');
grid on;
set(gca, 'box', 'off');


%% NO

% Set up random data for demonstration purposes
time_steps = 1:24;
point_forecast = rand(1, 24) * 20; % Increase variation in point forecast
quantiles = [0.4, 0.5, 0.6]; % Quantiles closer to the median
probabilistic_forecast = quantile(rand(1000, 24), quantiles);

% Create the first figure for the point forecast
figure('Position', [100, 100, 900, 400]);
plot(time_steps, point_forecast, 'LineWidth', 2);
xlabel('Time (hours)');
ylabel('Point Forecast');
title('24-hour Point Forecast');
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Create the second figure for the probabilistic forecast
figure('Position', [100, 100, 900, 400]);

% Define colors for each quantile range (shades of blue)
colors = [0.6, 0.6, 1.0];

% Plot shaded areas for quantile ranges
for i = 1:length(quantiles) - 1
    fill([time_steps, fliplr(time_steps)], [probabilistic_forecast(i, :), fliplr(probabilistic_forecast(i + 1, :))], colors, 'LineStyle', 'none');
    hold on;
end

% Plot the median line
plot(time_steps, probabilistic_forecast(ceil(end/2), :), 'LineWidth', 2, 'Color', 'k');
xlabel('Time (hours)');
ylabel('Probabilistic Forecast');
title('24-hour Probabilistic Forecast (Multiple Quantile Ranges)');
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Define the legend
lgd = legend('40%-60% Quantile Range', '50% Quantile (Median)');
title(lgd, 'Quantiles');
hold off;

%% Plotting point and probabilistic forecast

% Set up random data for demonstration purposes
time_steps = 1:24;
point_forecast = rand(1, 24) * 20; % Increase variation in point forecast
quantiles = [0.1, 0.3, 0.5, 0.7, 0.9];
probabilistic_forecast = quantile(rand(1000, 24) .* repmat((1:24), 1000, 1), quantiles); % Variable size depending on the timestep

% Create the first figure for the point forecast
figure('Position', [100, 100, 900, 400]);
plot(time_steps, point_forecast, 'LineWidth', 2);
xlabel('Hour of the Day','FontSize',14);
ylabel('Wind Power Production [W]','FontSize',14);
title('24-hour Point Forecast','FontSize',16);
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Create the second figure for the probabilistic forecast
figure('Position', [100, 100, 900, 400]);

% Define colors for each quantile range (shades of blue)
colors = [0.8, 0.8, 1.0;
          0.6, 0.6, 1.0];

% Plot shaded areas for quantile ranges
for i = 1:(length(quantiles) - 1) / 2
    fill([time_steps, fliplr(time_steps)], [probabilistic_forecast(i, :), fliplr(probabilistic_forecast(end - i + 1, :))], colors(i, :), 'LineStyle', 'none');
    hold on;
end

% Plot the median line
plot(time_steps, probabilistic_forecast(ceil(end/2), :), 'LineWidth', 2, 'Color', 'k');
xlabel('Hour of the Day','FontSize',14);
ylabel('Wind Power Production [W]','FontSize',14);
title('24-hour Probabilistic Forecast (Multiple Quantile Ranges)','FontSize',16);
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Define the legend
lgd = legend('10%-90% Quantile Range', '30%-70% Quantile Range', '50% Quantile (Median)');
lgd.FontSize = 14;
lgd.Location = 'best';
title(lgd, 'Quantiles');
hold off;


%%

% Set up random data for demonstration purposes
time_steps = 1:24;
point_forecast = rand(1, 24) * 20; % Increase variation in point forecast
quantiles = [0.1, 0.3, 0.5, 0.7, 0.9];

% Create a random matrix with more random quantile size increases
random_multiplier = 1 + 0.5 * rand(1000, 24);
probabilistic_forecast = quantile(rand(1000, 24) .* random_multiplier, quantiles);

% Create the first figure for the point forecast
figure('Position', [100, 100, 900, 400]);
plot(time_steps, point_forecast, 'LineWidth', 2);
xlabel('Time (hours)');
ylabel('Point Forecast');
title('24-hour Point Forecast');
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Create the second figure for the probabilistic forecast
figure('Position', [100, 100, 900, 400]);

% Define colors for each quantile range (shades of blue)
colors = [0.8, 0.8, 1.0;
          0.6, 0.6, 1.0];

% Plot shaded areas for quantile ranges
for i = 1:(length(quantiles) - 1) / 2
    fill([time_steps, fliplr(time_steps)], [probabilistic_forecast(i, :), fliplr(probabilistic_forecast(end - i + 1, :))], colors(i, :), 'LineStyle', 'none');
    hold on;
end

% Plot the median line
plot(time_steps, probabilistic_forecast(ceil(end/2), :), 'LineWidth', 2, 'Color', 'k');
xlabel('Time (hours)');
ylabel('Probabilistic Forecast');
title('24-hour Probabilistic Forecast (Multiple Quantile Ranges)');
xlim([1, 24]);
xticks(1:1:24);
grid on;

% Define the legend
lgd = legend('10%-90% Quantile Range', '30%-70% Quantile Range', '50% Quantile (Median)');
title(lgd, 'Quantiles');
hold off;


% Extract the data for timestep 2
data_timestep_2 = probabilistic_forecast(:, 2);

% Estimate the PDF and CDF using ksdensity function
[PDF, X] = ksdensity(data_timestep_2);
CDF = cumsum(PDF) * (X(2) - X(1));

% Create the first figure for the PDF
figure('Position', [100, 100, 600, 600]);
plot(X, PDF, 'LineWidth', 2);
xlabel('');
ylabel('Probability Density');
title('PDF for Timestep 2');
grid on;
set(gca, 'box', 'off');

% Create the second figure for the CDF
figure('Position', [100, 100, 600, 600]);
plot(X, CDF, 'LineWidth', 2);
xlabel('');
ylabel('Cumulative Probability');
title('CDF for Timestep 2');
grid on;
set(gca, 'box', 'off');


%%

% Create a vector of x values
x = linspace(0, 1, 1000);

% Compute the CDF of the Dirac delta function centered at 0.5
cdf_delta = double(x >= 0.5);

% Plot the CDF
figure;
plot(x, cdf_delta, 'Color', [0, 0.5, 0], 'LineStyle', '--', 'LineWidth', 3);
xlabel('x', 'FontSize', 24);
ylabel('CDF', 'FontSize', 24);
set(gca, 'box', 'off', 'FontSize', 20, 'LineWidth', 1.5);



% Create a vector of x values
x = linspace(0, 1, 1000);

% Compute the CDF of a uniform distribution from 0 to 1
cdf_uniform = x;

% Plot the CDF
figure;
plot(x, cdf_uniform, '--', 'Color', [0.85, 0.33, 0.1], 'LineWidth', 3);
xlabel('x', 'FontSize', 24);
ylabel('CDF', 'FontSize', 24);
set(gca, 'box', 'off', 'FontSize', 20, 'LineWidth', 1.5);

%% NO

% Create a range of x values
x = -5:0.01:5;

% Specify the mean and standard deviation for a Normal distribution
mu = 0; 
sigma = 1;

% Calculate the CDF
cdf_values = normcdf(x, mu, sigma);

% Plot the CDF
figure; 
plot(x, cdf_values,'LineWidth',2);
xlabel('x','FontSize',14);
ylabel('CDF','FontSize',14);
% title('Cumulative Distribution Function');

% Choose a specific point to highlight
x_point = 1;
cdf_point = normcdf(x_point, mu, sigma);

% Add the point to the plot
hold on;
plot(x_point, cdf_point, 'ro');
legend('CDF','Selected Point');
hold off;





%% 

% Create a range of x values
x = -3:0.01:3;

% Specify the mean and standard deviation for a Normal distribution
mu = 0; 
sigma = 1;

% Calculate the CDF
cdf_values = normcdf(x, mu, sigma);

% Plot the CDF
figure; 
plot(x, cdf_values,'LineWidth',2);
xlabel('x','FontSize',14);
ylabel('CDF','FontSize',14);
% title('Cumulative Distribution Function');

% Choose an interval to highlight
x_interval = [-1, 1];
cdf_interval = normcdf(x_interval, mu, sigma);

% Add the interval to the plot
hold on;
fill([x_interval, fliplr(x_interval)], [0, 0, fliplr(cdf_interval)], 'r', 'FaceAlpha', 0.1, 'EdgeColor', 'none');
hold off;

% Set the font size for the axis
set(gca, 'FontSize', 14);

% Label the axes
xlabel('x', 'FontSize', 16);
ylabel('CDF', 'FontSize', 16);

% Release the hold on the plot
hold off;

%%

% Define range of x and the selected point
x = -3:0.01:3;
x_selected = 1;

% Define the CDF for the standard normal distribution
cdf = normcdf(x, 0, 1);

% Create the figure
figure;

% Plot the CDF
plot(x, cdf, 'LineWidth', 2);

% Hold on to the plot
hold on;

% Plot the dotted lines
plot([x_selected, x_selected], [0, normcdf(x_selected, 0, 1)], 'r--', 'LineWidth', 2);
plot([-3, x_selected], [normcdf(x_selected, 0, 1), normcdf(x_selected, 0, 1)], 'r--', 'LineWidth', 2);

% Plot the selected point
plot(x_selected, normcdf(x_selected, 0, 1), 'ro', 'MarkerSize', 10, 'LineWidth', 2);

% Set the font size for the axis
set(gca, 'FontSize', 14);

% Label the axes
xlabel('x', 'FontSize', 16);
ylabel('CDF', 'FontSize', 16);

% Release the hold on the plot
hold off;

% box off

