mc_it=1;

%%

% load('WindRealizationForecast1')
% 
% addit = 0;
% 
% % No payment 
% for itt = 1:numOfScenarios
%     addit = addit + (1.*(  resultsAll(mc_it,1).lambda_RT(itt,1)/(1/numOfScenarios)*((realization*UpperGenLimit/BasePower)- resultsAll(mc_it,1).WindDayAheadPower_pu)));
% end
% 
% PayWindFarm = sum( resultsAll(mc_it,1).lambda_dayAhead(1,1)*resultsAll(mc_it,1).WindDayAheadPower_pu  + (1/numOfScenarios)*addit )
% 
% % totalPayment(mc_it,ii) = totalPayment1(mc_it,ii) + (1-crps)*lambda_dayAhead(1,1);

%%

%Optimal Payment 

% load('WindRealizationForecastOpt')

addit = 0;

% pay_ind = 30;

for l =1:length(pay_concat)

pay_ind = l;

payExpr = realization*resultsAll(mc_it,pay_ind).lambda_dayAhead(1,1)*(1-crps_it(mc_it,pay_ind))

for itt = 1:numOfScenarios
    addit = addit + (1.*(  resultsAll(mc_it,pay_ind).lambda_RT(itt,1)/(1/numOfScenarios)*((realization*UpperGenLimit/BasePower)- resultsAll(mc_it,pay_ind).WindDayAheadPower_pu)));
end

PayWindFarm(l) = sum( resultsAll(mc_it,pay_ind).lambda_dayAhead(1,1)*resultsAll(mc_it,pay_ind).WindDayAheadPower_pu  + (1/numOfScenarios)*addit ) +payExpr 

end


figure
% plot(pay_concat,PayWindFarm)
plot(pay_scaledInLambda, PayWindFarm)