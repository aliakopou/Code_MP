clear all
% close all
clear variables
% clc
%% Network characteristics

BaseVA = 100*10^6; 
BasePower = 100*10^6;

x12 = 0.1; %pu
x13 = 0.3; %pu
x23 = 0.1; %pu


B = [(1/x12 + 1/x13) -1/x12 -1/x13
    -1/x12 (1/x12 + 1/x23) -1/x23
    -1/x13 -1/x23 (1/x13 + 1/x23)];

rng(12) %seed for rand function below

PL = 150 *10^6 * ones(1,24);
% PL = 150 * ones(1,24).*rand(1,24);


N = 3; % number of buses 
NL = 3; % number of buses 
timestep = 24; %number of discrete time points
scenarios = 1; %number of scenarios

%% Parameters/Limits

Pg1_max = 200*10^6; 

rng(12) % seed for random generator
randomVariations = rand(1,24);

Pg2_max = 30*10^6.*ones(1,24);
% Pg2_max = 30*10^6.*randomVariations;

Pij_max = 400*10^6;

c1 = 60;
c2 = 0;

%% YALMIP Variable Declaration

theta = sdpvar(N,timestep,scenarios,'full');
% Pij = sdpvar(NL,timestep,scenarios,'full');
Pg1 = sdpvar(1,timestep,scenarios,'full');
Pg2 = sdpvar(1,timestep,scenarios,'full');
% Vi = sdpvar(N,timestep,scenarios,'full');
% Pg_da = sdpvar(1,timestep,'full');
% Pwp_da = sdpvar(1,timestep,'full');
Pg1_rt = sdpvar(1,timestep,scenarios,'full');
% Pspill = sdpvar(1,timestep,scenarios,'full');
% Pshed = sdpvar(1,timestep,scenarios,'full');


%% Define Problem Constraints 

Constraints = [];

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, theta(1,tim,sc) == 0]; %reference angle
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, Pg1(1,tim,sc) <= Pg1_max/BasePower];
        Constraints = [Constraints, Pg2(1,tim,sc) <= Pg2_max(tim)/BasePower];
        Constraints = [Constraints, Pg1(1,tim,sc) >= 0];
        Constraints = [Constraints, Pg2(1,tim,sc) >= 0];
    end
end


for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim,sc); Pg2(1,tim,sc); -PL(tim)/BasePower]] ;
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, theta(1,tim,sc) - theta(2,tim,sc) <= Pij_max/BasePower];
        Constraints = [Constraints, theta(2,tim,sc) - theta(3,tim,sc) <= Pij_max/BasePower];
        Constraints = [Constraints, theta(1,tim,sc) - theta(3,tim,sc) <= Pij_max/BasePower];
        Constraints = [Constraints, -(theta(1,tim,sc) - theta(2,tim,sc)) <= Pij_max/BasePower];
        Constraints = [Constraints, -(theta(2,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
        Constraints = [Constraints, -(theta(1,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
    end
end



%% Objective Function


disp('here')

% p1 = 0.4;
% p2 = 0.6; 

% cg = 20;
% cs = 30; 
% cg_2 = 100;

Objective = sum(sum(c1*Pg1(1,:,:) + c2*Pg2(1,:,:))); 


% options = sdpsettings('verbose',1,'solver','ipopt');
options = sdpsettings('verbose',1,'solver','gurobi');

%Solving the problem
sol = optimize(Constraints,Objective,options);


cal_time = sol.solvertime;


%% Results

size(Objective)
value(Objective)
% value(Pg1*BasePower)
% value(Pg2*BasePower)
