% clear all
clearvars -except xpoint_mean crps_mean
close all

%% Define Constants

UpperGenLimit = 2*150*10^6;

y_value = 0.5;     

% realizedPower = 150*10^6;

lambda_DA = 60;
% lambda_dayAheadRT = 100;

% b = lambda_DA;  %(range: 1 to lambda_DA)

% realization = realizedPower/UpperGenLimit;
% realization = 0.7;
% x_point_init = 0.5; % Set the value for x_point_init (the median of the probability distribution (PDF) that the given forecast (CDF) represents)

% realizedPower = realization*UpperGenLimit;

numOfScenarios = 100;

%% Set code parameters 
seedBool = 1;

use_payment_scaled = 0; %if set to 1: give payment values between 1 and lambda

includeOptimization = 1; %if set equal to 1 then SOPF problem is solved

plotCDF=0;

relationship_payment_b = 3;

useExistingxpoint = 1; 


%% Define parameter sweep values

%-- Payment --%
% payRange1 = 1:1:50;
%         btemp2 = [100 500 1000 1500 2000];
% payRange2 = 50:1:100;
%     btemp2 = 100;
% payRange3 = 100:100:2000;
pay_concat = 1;  %the payment values

% pay_concat = (1:10:2000);

% pay_concat = (1:1:2000);

% pay_concat = (1:100:2000);

%-- Realization --%
% rangg = [0.2,0.4,0.6,0.8];

rangg = 0.6;

%%

sc1 = 1;

% mc_it = 1; %monte carlo iteration number
% for mc_it=1:size(rangg,2) %monte carlo iterations
for numOfScenarios = [10,100,500,1000,2000,3000,4000,5000,10000,20000,50000,100000]
% for numOfScenarios = [10,100,1000]

tic 

    for mc_it=1:1

    %     iterr = 1;

    realization = rangg(1);
    realizedPower = realization*UpperGenLimit;

    xalready1 = realization; % so that there is no epsilon and hence no extra randomness

    if seedBool == 1
        % randSeedNo = randi(10000);
        randSeedNo = 10;
    else 
        randSeedNo = [];
    end

    for ii = 1:size(pay_concat,2)
        %             for ii = 10:10
        %     for b = linspace(1,10,100)
        %     for b = linspace(1,2,10)
        %     close all

        payment = pay_concat(ii);

        %-- Define relationship between payment and b --%
        if relationship_payment_b == 1   %linear realtionship
            b = payment;
        elseif relationship_payment_b == 2   %exponential relationship
            min_payment = 1;
            max_payment = 2000;
            min_b = 1;
            max_b = 2000;

            %             base = 3; %the larger, the faster b increases
            curvature = 3;

            %         for ii = 1:1:2000
            % Normalize the payment value to the range [0, 1]
            %             normalized_payment = (pay_concat(ii) - min_payment) / (max_payment - min_payment);
            scaling_factor = (log(max_b / min_b)) / ((max_payment - min_payment)^curvature);

            % Apply the exponential relationship
            %             b = min_b + (1 - exp(-decay_constant * normalized_payment * (max_payment - min_payment))) * (max_b - min_b)
            b = min_b * exp(scaling_factor * (payment - min_payment)^curvature);
            %         end

            %          figure
            %          plot(1:length(b),b)
        else   %logarithmic relationship
            min_payment = 1;
            max_payment = 2000;

            min_b = 1;
            max_b = 2000;

            decay_constant = 7;

            % for ii = 1: 1:2000
            % Normalize payment to a range of [0, 1]
            normalized_payment = (payment - min_payment) / (max_payment - min_payment);

            % Logarithmic relationship between payment and b
            b = min_b + (log(1 + normalized_payment * (exp(decay_constant) - 1)) / log(exp(decay_constant))) * (max_b - min_b);
            % end
        end

        %-- Obtain the probabilistic wind forecast --%
        if useExistingxpoint==1
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled,useExistingxpoint,xalready1(mc_it,ii));
            
            % x_point = xalready1(mc_it,ii);
        else 
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled,useExistingxpoint,[]);
        end
        %     [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);
        %
        %     disp(['The mean of the forecast distribution for this b value is equal to ', num2str(x_point)])

        %-- Run SOPF using the probabilistic wind forecast --%
        if includeOptimization == 1
            %             resultsSOPF = StochOptimization_revisited_singleTimestep(scens,realizedPower);
            StochOptimization_revisited_singleTimestep;

            resultsSOPF = resultsOpt;

            resultsSOPF.realization = realization;

            resultsSOPF.realizedPower = realizedPower;

            resultsSOPF.pay_concat = pay_concat;

            resultsSOPF.b = b;
            resultsSOPF.useb_scaled = use_payment_scaled;

            resultsAll(mc_it,ii) = resultsSOPF;

            lambda_DA = lambda_dayAhead(1,1);

            %             lambda_RT(:,:,mc_it,ii) = lambda_RT;
            %             clc
        end


        %-- Compute crps value --%
        crps = computingCRPS_usingAnalyticalFormula(5, lambda_DA, b, y_value, realization, x_point, plotCDF, use_payment_scaled);

        %     crps = computingCRPS_usingAnalyticalFormula(4,lambda_DA,b,y_value,realization,x_point_init);


        if includeOptimization == 1
            addit = 0;

            for itt = 1:numOfScenarios
                addit = addit + (1.*(  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/UpperGenLimit  -  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/UpperGenLimit  ));
            end

            totalPayment1(mc_it,ii) = sum( lambda_dayAhead(1,1)*GeneratorDayAheadPower/UpperGenLimit  + lambda_dayAhead(1,1)*WindDayAheadPower/UpperGenLimit  + (1/numOfScenarios)*addit );

            totalPayment(mc_it,ii) = totalPayment1(mc_it,ii) + (1-crps)*lambda_dayAhead(1,1);


            obj_it(sc1,ii) = objectiveFunctionValue

        end

        crps_it(sc1,ii) = crps;

        epsilon_it(sc1,ii) = random_epsilon;

        b_it(sc1,ii) = b;
        
        if useExistingxpoint==0
            x_point_it(sc1,ii) = x_point;
        end

        sigma_it(sc1,ii) = current_sigma;
        
        if seedBool == 1
            randSeedNo_it(sc1,ii) = randSeedNo;
        end


        %     close all


        mc_it; %monte carlo iteration number


        %         iterr = iterr +1;

    end


    
    end


    iterationsTotalTime(sc1) = toc;

    sc1 = sc1+1

end






