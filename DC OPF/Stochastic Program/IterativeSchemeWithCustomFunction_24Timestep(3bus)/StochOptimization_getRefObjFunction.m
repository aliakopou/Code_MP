function [objectiveFunctionValue,cal_time] = StochOptimization_getRefObjFunction(realizedPower)

% clear all
% close all
% clear variables 
% clc
yalmip('clear')
%% Network characteristics

BaseVA = 100*10^6;
BasePower = 100*10^6;

x12 = 0.1; %pu
x13 = 0.3; %pu
x23 = 0.1; %pu


B = [(1/x12 + 1/x13) -1/x12 -1/x13
    -1/x12 (1/x12 + 1/x23) -1/x23
    -1/x13 -1/x23 (1/x13 + 1/x23)];


rng(12) % seed for random generator

PL = (250/130)*realizedPower*10^6 * ones(1,24);  %scaling factor could be anything (here 250/130)
% PL = 150*10^6.*rand(1,24);

N = 3; % number of buses
NL = 3; % number of buses
timestep = 24; %number of discrete time points

%% Parameters/Limits

Pg1_max = (300/130)*realizedPower*10^6; %scaling factor could be anything

Pij_max = (500/130)*realizedPower*10^6; %scaling factor could be anything

it1 = 0;

% for iterVal = 32*10^6 :1*10^5:32*10^6
for iterVal = 1 :1:1
    
    % WindGenerationRealised = Pg2_max * ones(1,24).*(rand(1,24)*10);
    % WindGenerationRealised = Pg2_max * ones(1,24) * 0.9;
    % WindGenerationRealised = Pg2_max * ones(1,24) * 1;
    
    
    %     % Pg2_max = 200*10^6; %essentially your forecast
    %     % WindRealizationForecast = [5000*10^6 ; 30*10^6];
    %     randomVariations = rand(1,24);
    % %     WindRealizationForecast = [iterVal.*rand(1,24) ; 30*10^6.*randomVariations; 1*10^6.*rand(1,24)];
    % %     WindRealizationForecast = [iterVal.* rand(1,24) ; 30*10^6.*randomVariations];
    %     WindRealizationForecast = [31*10^6.*randomVariations ; 30*10^6.*randomVariations];
    %     [maxWind, indexMax] = max(WindRealizationForecast);
    %
    %     scenarios = size(WindRealizationForecast,1); %number of scenarios
    
    
    %     mu = 30;
    %     sigma = iterVal;
    %     sz = [1,100000];
    
    %     scens = SamplingScenarios(mu,sigma,sz);
    
    %     WindRealizationForecast = transpose(scens.BinEdges(2:end))*10^6*ones(1,24);
    %     WindRealizationForecast = transpose(scens.BinEdges(2:end))*10^6*ones(1,24)+ones(length(scens.BinEdges(2:end)),1)*randn(1,24);
    rng(12) % seed for random generator
    WindRealizationForecast = realizedPower*10^6*ones(1,24).*rand(1,24);
    
    
    maxWind = WindRealizationForecast;
    indexMax = 1;
    
    scenarios = size(WindRealizationForecast,1); %number of scenarios
    
    p = 1;
    
    
    %% Costs Definition
    cg = 60; % the generator 1 cost
    % c2 = 120;
    c_rt = 100; % the regualtion cost (has to be more expensive than cg)
    cs = 650;  % the load shedding cost
    
    %% YALMIP Variable Declaration
    
    theta = sdpvar(N,timestep,scenarios,'full');
    % Pij = sdpvar(NL,timestep,scenarios,'full');
    Pg1 = sdpvar(1,timestep,1,'full');   %Day Ahead generator power
    Pg2 = sdpvar(1,timestep,1,'full');   %Day Ahead wind power
    % Vi = sdpvar(N,timestep,scenarios,'full');
    % Pg_da = sdpvar(1,timestep,'full');
    % Pwp_da = sdpvar(1,timestep,'full');
    Pg1_rt = sdpvar(1,timestep,scenarios,'full');
    Pspill = sdpvar(1,timestep,scenarios,'full');
    Pshed = sdpvar(1,timestep,scenarios,'full');
    
    
    %% Define Problem Constraints
    
    Constraints = [];
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, theta(1,tim,sc) == 0]; %reference angle
        end
    end
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, Pg1(1,tim) <= Pg1_max/BasePower];
            Constraints = [Constraints, Pg2(1,tim) <= maxWind(tim)/BasePower];
            Constraints = [Constraints, Pg1(1,tim) >= 0];
            Constraints = [Constraints, Pg2(1,tim) >= 0];
            Constraints = [Constraints, Pg1(1,tim) + Pg1_rt(1,tim,sc) <= Pg1_max/BasePower];
            Constraints = [Constraints, Pg1(1,tim) + Pg1_rt(1,tim,sc) >= 0];
            %         Constraints = [Constraints, Pg1_rt(1,tim,sc) <= Pg1_max/BasePower];
            %         Constraints = [Constraints, Pg1_rt(1,tim,sc) >= -Pg1_max/BasePower];
        end
    end
    
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower]] ;
        end
    end
    
    % for sc = 1:scenarios
    %     for tim = 1:timestep
    %         Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim)+Pg1_rt(1,tim,sc); Pg2(1,tim)-1*Pspill(1,tim,sc); -PL(tim)/BasePower+Pshed(1,tim,sc)]] ;
    %     end
    % end
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, (1/x12)*(theta(1,tim,sc) - theta(2,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta(2,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta(1,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x12)*(theta(1,tim,sc) - theta(2,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta(2,tim,sc) - theta(3,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta(1,tim,sc) - theta(3,tim,sc)) >= -Pij_max/BasePower];
        end
    end
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, 1*Pspill(1,tim,sc) <= WindRealizationForecast(sc,tim)/BasePower];
            Constraints = [Constraints, Pshed(1,tim,sc) <= PL(tim)/BasePower];
            Constraints = [Constraints, 1*Pspill(1,tim,sc) >= 0];
            Constraints = [Constraints, Pshed(1,tim,sc) >= 0];
        end
    end
    
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, Pg1_rt(1,tim,sc)+ Pshed(1,tim,sc) + (WindRealizationForecast(sc,tim)/BasePower - Pg2(1,tim) - 1*Pspill(1,tim,sc)) == 0]; %We want the deviations from the day ahead values to be zero
        end
    end
    
    %% Objective Function
    
    
    disp('Constraint definition has finished')
    
    %     p1 = 0.1;
    %     p2 = 0.9;
    %     p3 = 0.2;
    
    %     p = [0.1; 0.9];
    
    % cg = 20;
    % cs = 30;
    % cg_2 = 100;
    
    % Objective = sum(cg*Pg1(1,:,:) + 0*Pg2(1,:,:));
    
    %     Objective = (sum(cg*Pg1(1,:) + ...
    %         p1*(c_rt*abs(Pg1_rt(1,:,1))+cs*Pshed(1,:,1)) +...
    %         p2*(c_rt*abs(Pg1_rt(1,:,2))+cs*Pshed(1,:,2))));
    % %         p3*(c_rt*abs(Pg1_rt(1,:,3))+cs*Pshed(1,:,3))));
    
    
    add1 = 0;
    
    for itt = 1:scenarios
        add1 = add1 + (p(itt).*(c_rt*abs(Pg1_rt(1,:,itt))+cs*Pshed(1,:,itt)));
    end
    
    Objective = sum(cg*Pg1(1,:) + add1);
    
    
    
    % options = sdpsettings('verbose',1,'solver','ipopt');
    options = sdpsettings('verbose',1,'solver','gurobi');
    % options = sdpsettings('verbose',1,'solver','bmibnb');
    
    %Solving the problem
    sol = optimize(Constraints,Objective,options);
    
    
    cal_time = sol.solvertime;
    
    it1 = it1+1;
    
    iterVal
    
    ObjIter(it1) = value(Objective);
    
    objectiveFunctionValue = value(Objective);
    
end


%% Results

% size(Objective)
% value(Objective)
% value(Pg1*BasePower)
% value(Pg2*BasePower)
% value(Pg1_rt*BasePower)
% value(Pshed*BasePower)
% value(Pspill*BasePower)
%
%
% figure
% plot(1:24, value(Pg1*BasePower))
% hold on
% plot(1:24, value(Pg2*BasePower))
% hold on
% plot(1:24, value(Pg1_rt*BasePower))
% hold on
% plot(1:24, value(Pshed*BasePower))
% hold on
% plot(1:24, value(Pspill*BasePower))
% xlim([1 23])
%
% figure
% plot(1:24, WindGenerationRealised)
% hold on
% plot(1:24, Pg2_max*ones(1,24))
% xlim([1 24])


%%
% figure
% plot(1:length(ObjIter),ObjIter)
% xlim([2 length(ObjIter)])

end