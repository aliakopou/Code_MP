clear all
close all
clear variables
yalmip('clear')
%%

initPayment = 1+1/625;
payment(1) = initPayment;
payment(2) = 1 + 0.15;

mu = 130;
sz = [1,100000];

% ref = 2.766550131946314e+03; %obtained by paying a very high payment value (here 100000)

loopIndex1 = 1;
l=1;
stopCriterion = 0;

% numOfScenarios = 100;
numOfScenarios = 20;

% realizedPower = 130; %value given in MW

%%

% numberOfBinsApproximation = 10;

% for numberOfBinsApproximation = [10,100,1000,10000,100000,1000000]
% for numberOfBinsApproximation = 10

% for realizedPower = [10, 130, 1000]
for realizedPower = 130 %value given in MW
    
    l=1;
    stopCriterion = 0;
    
    initPayment = 1+1/625;
    payment(1) = initPayment;
    payment(2) = 1 + 0.15;
    
    [ref,calc1] = StochOptimization_getRefObjFunction(realizedPower); %calculate objective function value when no uncertainty
    
    %%--Compute Realization
    MaxWind = 2*130*10^6;
    
    %     rng(12) % seed for random generator
    %     WindRealization = realizedPower*10^6*ones(1,24).*rand(1,24);
    %     WindRealization = realizedPower*10^6*ones(1,24);
    numberSamples = 1;
    powerVal = 0.5 * MaxWind;
    
%     visualizeCustomCDF(b,x_point,y_value)
    
    for nn = 1:24
        [res1,res2] = getScenariosUsingInverseTransformSampling(2000,numberSamples,powerVal,0);
        SamplesAll(nn) = res1;
    end
    WindRealization = SamplesAll;
    
    
    %     while stopCriterion==0
%     for itVal=[0.1,0.5,0.8,1,1.4,2,2000]
    for itVal=[2,20,200,500,2000]
%     for itVal=[2,2000]    
       
        % sigma = iterVal;
        %     if l>=3
        %         sigma = getSigmaOfDistribution(abs(payment(l-1)));
        %     else
        %         sigma = getSigmaOfDistribution(payment(l));
        %     end
        
        %     scens = SamplingScenarios(mu,sigma,sz,numberOfBinsApproximation);
        
        %     b = itVal;
        
        
        alpha = 1; %learning rate
        
        if l>=3
            der(l) = ( ((objFunctionValue(l-2)- (payment(l-2)))-ref) - ((objFunctionValue(l-1)- (payment(l-1)))-ref) ) / (  (payment(l-2)) - ( (payment(l-1))) );
            
            payment(l) =  (payment(l-1)) - alpha*( ((objFunctionValue(l-1)- (payment(l-1)))-ref) /der(l));
        end
        
        
        %     if l>=3
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l-1), numOfScenarios);
        %     else
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios);
        % %         [scens,probs] = getScenariosFromCDFfunction(300, numOfScenarios);
        %     end
        
        %%--Compute new CDF value given the payment and then get the PDF and compute wind power scenarios values that then gives them into the stoch optimiz problem--%%--%%
        
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios,realizedPower);
        [scens,probs] = getScenariosFromCDFfunction(itVal, numOfScenarios,realizedPower);
        
        
        [objFunctionValueOut,optimSolverTime,lambda_dayAhead,c_rt] = StochOptimization_scensFromCDFfunction(scens,probs,realizedPower);
        
        %     if objFunctionValueOut==0  %returned when the optim problem is infeasible
        %         objFunctionValueOut = ref*2; %a very big value
        %     end
        
        
        objFunctionValue(l) = objFunctionValueOut;
        
        
        
        
        %%--Gradient Descent Implementation--%%
        
        %     if l==1
        %         payment(l) = initPayment;
        %     elseif l>=2
        %         payment(l) = payment(l-1) - alpha*(((objFunctionValue(l)+payment(l-1))-(objFunctionValue(l-1)+payment(l-1)))/(payment(l-1)));
        %     end
        
        
        %     if payment(l) < 0
        %         payment(l) = payment(l-1);
        %     end
        if l>3
            if ( ( ( objFunctionValue(l-1)- (payment(l-1)) )-ref ) - ( ( objFunctionValue(l)- (payment(l)) )-ref ) )<=0.5 && ( ((objFunctionValue(l-1)- (payment(l-1)))-ref)-((objFunctionValue(l)- (payment(l)))-ref) )>=0
                stopCriterion =1;
            end
        end
        
        
        payment
        objFunctionValue
        l
        
        paymentsArray(l) = itVal; 
        
        solverTimeVaryingNumberOfScens(l) = optimSolverTime;
        
%         MaxWind = 1.1*max(WindRealization);
        for nn = 1:24
            CRPS_values_All(l,nn) = crps_calculate(itVal,WindRealization(nn),0.5,0.5,MaxWind);
        end
        
        l=l+1;
        
        %     end
    end
    
    AllResults(loopIndex1).realizedPower = realizedPower;
    AllResults(loopIndex1).objFunctionValue = objFunctionValue;
    AllResults(loopIndex1).payment = payment;
    AllResults(loopIndex1).solverTimeVaryingNumberOfScens = solverTimeVaryingNumberOfScens;
    
    loopIndex1 = loopIndex1 +1;

    
end

%%

% paytemp = [0.1,0.5,0.8,1,1.4,2,2000];
% paytemp = [2,20,200,500,2000];

% for nnn = 1:length(paymentsArray)
%     vals1(nnn) =  (1-sum(CRPS_values_All(nnn,:))/24)*paymentsArray(nnn);
% end
% 
% figure
% plot(vals1,objFunctionValue+vals1)


for nnn = 1:length(paymentsArray)
    totalTotalCost(nnn) = objFunctionValue(nnn) + (sum(lambda_dayAhead(1).scen1(1,1))*24* realizedPower + 1/2 * c_rt * sum(CRPS_values_All(nnn,:))) + paymentsArray(nnn)
end

figure
plot(paymentsArray,totalTotalCost)

%CRPS over payment values
figure
for nnn = 1:length(paymentsArray)
plot(paymentsArray(nnn),sum(CRPS_values_All(nnn,:)))
hold on
end