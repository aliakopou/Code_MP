function sigma = getSigmaOfDistribution(payment)

sigma = 1/sqrt(payment);  %for now function is 1/sqrt(x)

if payment<=0
    sigma = 100;
end

end