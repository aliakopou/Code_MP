clear all
close all

%%

initPayment = 1/625;
payment(1) = initPayment;
payment(2) = 0.15;

mu = 130;
sz = [1,100000];

ref = 2.751771881e+03;

l=1;
stopCriterion = 0;

numberOfBinsApproximation = 10;

% for numberOfBinsApproximation = [10,100,1000,10000,100000,1000000]
for numberOfBinsApproximation = 10
    
    % while stopCriterion==0
%     for l=1:1
        
        
        
        % sigma = iterVal;
        if l>=3
            sigma = getSigmaOfDistribution(abs(payment(l-1)));
        else
            sigma = getSigmaOfDistribution(payment(l));
        end
        
        scens = SamplingScenarios(mu,sigma,sz,numberOfBinsApproximation);
        
        [objFunctionValueOut,optimSolverTime] = StochOptimization(scens);
        
        if objFunctionValueOut==0
            objFunctionValueOut = ref*2; %a very big value
        end
        
        objFunctionValue(l) = objFunctionValueOut;
        
        alpha = 1;
        
        %%Gradient Descent Implementation
        
        %     if l==1
        %         payment(l) = initPayment;
        %     elseif l>=2
        %         payment(l) = payment(l-1) - alpha*(((objFunctionValue(l)+payment(l-1))-(objFunctionValue(l-1)+payment(l-1)))/(payment(l-1)));
        %     end
        if l>=3
            der(l) = ( ((objFunctionValue(l-2)-abs(payment(l-2)))-ref) - ((objFunctionValue(l-1)-abs(payment(l-1)))-ref) ) / ( abs(payment(l-2)) - (abs(payment(l-1))) );
            
            payment(l) = abs(payment(l-1)) - alpha*( ((objFunctionValue(l-1)-abs(payment(l-1)))-ref) /der(l));
        end
        
        %     if payment(l) < 0
        %         payment(l) = payment(l-1);
        %     end
        if l>3
            if (((objFunctionValue(l-1)-abs(payment(l-1)))-ref)-((objFunctionValue(l)-abs(payment(l)))-ref))<=0.5 && (((objFunctionValue(l-1)-abs(payment(l-1)))-ref)-((objFunctionValue(l)-abs(payment(l)))-ref))>=0
                stopCriterion =1;
            end
        end
        
        
        payment
        objFunctionValue
        l
        
        
        solverTimeVaryingNumberOfScens(l) = optimSolverTime;
        
        l=l+1;
%     end
end
