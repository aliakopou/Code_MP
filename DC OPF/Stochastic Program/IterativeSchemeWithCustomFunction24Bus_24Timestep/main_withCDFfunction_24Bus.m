clear all
close all
clear variables
yalmip('clear')
%%

initPayment = 1+1/625;
payment(1) = initPayment;
payment(2) = 1 + 0.15;

% mu = 130;
% sz = [1,100000];

% ref = 2.766550131946314e+03; %obtained by paying a very high payment value (here 100000)

loopIndex1 = 1;
l=1;
stopCriterion = 0;

numOfScenarios = 10;

%%

% numberOfBinsApproximation = 10;

% for numberOfBinsApproximation = [10,100,1000,10000,100000,1000000]
% for numberOfBinsApproximation = 10

% for realizedPower = [10, 130, 1000]
for realizedPower = 150*10^6
    
    l=1;
    stopCriterion = 0;
    
    initPayment = 1+1/625;
    payment(1) = initPayment;
    payment(2) = 1 + 0.15;
    
    [ref,calc1] = StochOptimization_getRefObjFunction_24Bus(realizedPower); %calculate objective function value when no uncertainty
    
    %     while stopCriterion==0
    for payVal=[2,20,200,1000,2000]
%     for payVal=2:1:2
        
        % sigma = iterVal;
        %     if l>=3
        %         sigma = getSigmaOfDistribution(abs(payment(l-1)));
        %     else
        %         sigma = getSigmaOfDistribution(payment(l));
        %     end
        
        %     scens = SamplingScenarios(mu,sigma,sz,numberOfBinsApproximation);
        
        %     logic that computes new cdf value given the payment and then gets the pdf and logic that computes wind power scenarios that then gives them into the stoch optimiz problem
        
        %     b = payVal;
        
        alpha = 1;
        if l>=3
            der(l) = ( ((objFunctionValue(l-2)- (payment(l-2)))-ref) - ((objFunctionValue(l-1)- (payment(l-1)))-ref) ) / (  (payment(l-2)) - ( (payment(l-1))) );
            
            payment(l) =  (payment(l-1)) - alpha*( ((objFunctionValue(l-1)- (payment(l-1)))-ref) /der(l));
        end
        
        
        
        %     if l>=3
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l-1), numOfScenarios);
        %     else
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios);
        % %         [scens,probs] = getScenariosFromCDFfunction(300, numOfScenarios);
        %     end
        
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios,realizedPower);
        [scens,probs] = getScenariosUsingInverseTransformSampling(payVal, numOfScenarios,realizedPower,0);
        %         [scens,probs] = getScenariosFromCDFfunction(payVal, numOfScenarios,realizedPower/10^6);
        
        
        [objFunctionValueOut,optimSolverTime] = StochOptimization_scensFromCDFfunction_24Bus(scens,probs,realizedPower);
        
        %     if objFunctionValueOut==0  %returned when the optim problem is infeasible
        %         objFunctionValueOut = ref*2; %a very big value
        %     end
        
        
        objFunctionValue(l) = objFunctionValueOut;
        paymentVal(l) = payVal;
        
        
        
        %%Gradient Descent Implementation
        
        %     if l==1
        %         payment(l) = initPayment;
        %     elseif l>=2
        %         payment(l) = payment(l-1) - alpha*(((objFunctionValue(l)+payment(l-1))-(objFunctionValue(l-1)+payment(l-1)))/(payment(l-1)));
        %     end
        
        
        %     if payment(l) < 0
        %         payment(l) = payment(l-1);
        %     end
        if l>3
            if (((objFunctionValue(l-1)- (payment(l-1)))-ref)-((objFunctionValue(l)- (payment(l)))-ref))<=0.5 && (((objFunctionValue(l-1)- (payment(l-1)))-ref)-((objFunctionValue(l)- (payment(l)))-ref))>=0
                stopCriterion =1;
            end
        end
        
        
        payment
        objFunctionValue
        l
        
        
        solverTimeVaryingNumberOfScens(l) = optimSolverTime;
        
        
        
        l=l+1;
        
        %     end
    end
    
    AllResults(loopIndex1).realizedPower = realizedPower;
    AllResults(loopIndex1).objFunctionValue = objFunctionValue;
    AllResults(loopIndex1).payment = payment;
    AllResults(loopIndex1).solverTimeVaryingNumberOfScens = solverTimeVaryingNumberOfScens;
    
    loopIndex1 = loopIndex1 +1;
end

%%
figure
plot( paymentVal,objFunctionValue-(ref.*ones(1,length(objFunctionValue)))+paymentVal )
