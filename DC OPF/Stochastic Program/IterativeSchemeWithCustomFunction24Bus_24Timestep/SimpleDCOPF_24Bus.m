clear all
close all
clear variables
% clc
yalmip('clear')
%% Network characteristics

BaseVA = 100*10^6;
BasePower = 100*10^6;

% Create B matrix
load('linedata_24Bus');
Y = Ymatrix_Create(linedataBus24,1,1);
B = -imag(Y);

%% Parameters/Limits

load('lineCapacity_24Bus');
Pij_max = lineCapacity_24Bus.*10^6; %multiply by 10^6 because values should be in MVA
% load('LineLimits_24Bus');
% Pij_max = LineLimits(:,3).*10^6; %multiply by 10^6 because values should be in MVA

% rng(12) % seed for random generator

load('LoadData_24Bus')

numberOfLoads = size(LoadData_24Bus.percentSystemLoadPerNode ,1);

LoadProfile = LoadData_24Bus.systemDemand.*10^6;  %load profile
PL = (LoadData_24Bus.percentSystemLoadPerNode(:,2)./100)*transpose(LoadProfile); %size of PL is (numberOfLoads,24)


N = 24; % number of buses
NL = LoadData_24Bus; % number of lines
timestep = 24; %number of discrete time points

windFarmNodes = [3; 5; 7; 16; 21; 23];

numberOfWindFarms = size(windFarmNodes,1);


load('GeneratorData_24Bus')
% load('GeneratorLimits_24Bus')


rng(12) % seed for random generator
WindRealizationForecast = (150*10^6)*ones(numberOfWindFarms,24)+10*10^6.*randn(numberOfWindFarms,24); %Assuming only 1 scenario 

maxWind= WindRealizationForecast; %equality because we have only a single scenario
% [maxWind, indexMax] = max(WindRealizationForecast); 

% scenarios = size(WindRealizationForecast,1); %number of scenarios
scenarios = 1; %number of scenarios

% p = [0.5 0.5];
p=1;




%% YALMIP Variable Declaration

theta = sdpvar(N,timestep,scenarios,'full');
% Pij = sdpvar(NL,timestep,scenarios,'full');
P_gen = sdpvar(length(GeneratorData_24Bus.generatorNodes),timestep,'full');   %Day Ahead generator power
P_wind = sdpvar(numberOfWindFarms,timestep,'full');   %Day Ahead wind power
% Vi = sdpvar(N,timestep,scenarios,'full');
% Pg_da = sdpvar(1,timestep,'full');
% Pwp_da = sdpvar(1,timestep,'full');
P_gen_rt = sdpvar(length(GeneratorData_24Bus.generatorNodes),timestep,scenarios,'full');
Pspill = sdpvar(numberOfWindFarms,timestep,scenarios,'full');
Pshed = sdpvar(numberOfLoads,timestep,scenarios,'full');


%% Define Problem Constraints

Constraints = [];

%Constraint for slack bus
for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, theta(13,tim,sc) == 0]; %reference angle
    end
end

%Constraint for min and max power output of generators
for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, P_gen(:,tim,1) <= GeneratorData_24Bus.generatorLimits(:,2).*10^6./BasePower];
        Constraints = [Constraints, P_gen(:,tim,1) >= GeneratorData_24Bus.generatorLimits(:,3).*10^6./BasePower];
        Constraints = [Constraints, P_gen(:,tim,1) + P_gen_rt(:,tim,sc) <= GeneratorData_24Bus.generatorLimits(:,2).*10^6./BasePower];
        Constraints = [Constraints, P_gen(:,tim,1) + P_gen_rt(:,tim,sc) >= GeneratorData_24Bus.generatorLimits(:,3).*10^6./BasePower];
        %         Constraints = [Constraints, Pg1_rt(1,tim,sc) <= Pg1_max/BasePower];
        %         Constraints = [Constraints, Pg1_rt(1,tim,sc) >= -Pg1_max/BasePower];
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, P_wind(:,tim) <= maxWind(:,tim)./BasePower];
        Constraints = [Constraints, P_wind(:,tim) >= 0];
        %         Constraints = [Constraints, Pg1_rt(1,tim,sc) <= Pg1_max/BasePower];
        %         Constraints = [Constraints, Pg1_rt(1,tim,sc) >= -Pg1_max/BasePower];
    end
end

% Create matrA: NxN matrix where matrA(n,n)=1 if there is generator
% connected to node n. Same goes for matrB and matrC but for wind farm and
% load respectively. 
matrA = zeros(N,size(P_gen,1)); %initialization
matrB = zeros(N,size(P_wind,1)); %initialization
matrC = zeros(N,size(PL,1)); %initialization
A_ind = 1;
B_ind = 1;
C_ind = 1;
for k = 1:N
    if ismember(k,GeneratorData_24Bus.generatorNodes)
        matrA(k,A_ind) = 1;
        A_ind = A_ind+1;
    end
    if ismember(k,LoadData_24Bus.percentSystemLoadPerNode(:,1))
        matrC(k,C_ind) = 1;
        C_ind = C_ind+1;
    end
    if ismember(k,windFarmNodes)
        matrB(k,B_ind) = 1;
        B_ind = B_ind+1;
    end 
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, B * theta(:,tim,sc) == matrA*  P_gen(:,tim) + matrB* P_wind(:,tim) + matrC* (-PL(:,tim)./BasePower)] ;
    end
end


for sc = 1:scenarios
    for tim = 1:timestep
        for in = 1:size(linedataBus24,1)
            Constraints = [Constraints, (1/linedataBus24(in,4))*(theta(linedataBus24(in,1),tim,sc) - theta(linedataBus24(in,2),tim,sc)) <= Pij_max(in)./BasePower];
      
            Constraints = [Constraints, (1/linedataBus24(in,4))*(theta(linedataBus24(in,1),tim,sc) - theta(linedataBus24(in,2),tim,sc)) >= -Pij_max(in)./BasePower];
        end
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, 1*Pspill(:,tim,sc) <= WindRealizationForecast(:,tim)./BasePower];
        Constraints = [Constraints, Pshed(:,tim,sc) <= PL(:,tim)./BasePower];
        Constraints = [Constraints, 1*Pspill(:,tim,sc) >= 0];
        Constraints = [Constraints, Pshed(:,tim,sc) >= 0];
    end
end


for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, sum(P_gen_rt(:,tim,sc)) + sum(Pshed(:,tim,sc)) + (sum(WindRealizationForecast(:,tim)./BasePower) - sum(P_wind(:,tim)) - 1*sum(Pspill(:,tim,sc))) == 0]; %We want the deviations from the day ahead values to be zero
    end
end


%% Costs Definition

costsGen = GeneratorData_24Bus.dayAheadOfferPrice; %day-ahead offer price for each generator
costsRT_Up = GeneratorData_24Bus.UpRegulationOfferPrice; %the regualtion price for each generator
costsRT_Down = GeneratorData_24Bus.DownRegulationOfferPrice;

costSheding = 200.*ones(numberOfLoads,1);  %the load shedding cost

%% Objective Function


disp('Constraint definition has finished')

add1 = 0;


% PAY ATTENTION: For objective function all variables are in pu so multiply
% by 10^6 to make them MW, since cost prices are given in dollar/MWh

for itt = 1:scenarios
    add1 = add1 + ( p(itt).*(sum(transpose(costsRT_Up)*abs(P_gen_rt(:,:,itt).*10^6)) + sum(transpose(costSheding)*(Pshed(:,:,itt).*10^6))) );
end

Objective = sum( sum(transpose(costsGen)*(P_gen(:,:).*10^6)) + add1 );



% options = sdpsettings('verbose',1,'solver','ipopt');
options = sdpsettings('verbose',1,'solver','gurobi');
% options = sdpsettings('verbose',1,'solver','bmibnb');

%Solving the problem
sol = optimize(Constraints,Objective,options);


cal_time = sol.solvertime;


objectiveFunctionValue = value(Objective);


