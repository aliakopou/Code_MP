function [finalScens,probabilitiesOfFinalScens] = getScenariosUsingInverseTransformSampling(b,numberOfScens,realizedPower,seedBool)

% b = 20000;
% numberOfScens = 100;
% realizedPower = 130*10^6;
% seedBool = 1;

%% Obtaining samples using 'Inverse Transform Sampling' method (https://en.wikipedia.org/wiki/Inverse_transform_sampling)

centerValueOfDistribution = realizedPower; % if we were to consider the distribution as a gaussian this would be the mean
% centerValueOfDistribution = 130*10^6; % if we were to consider the distribution as a gaussian this would be the mean

if seedBool == 1 %use a seed
    rng(12)
end
vector1 = rand(1,numberOfScens); %sampling from Unif[0,1]

for i = 1:length(vector1)
    x_fromInverse(i) = 1/(nthroot((1/vector1(i) - 1),b)+1); %obtain normalized samples (wind power values) using the analytical expression of the inverse CDF function
end

samples = x_fromInverse*(centerValueOfDistribution*2);                                        
% samples = x_fromInverse;

% finalScens = samples;

%Plot the range of values
if numberOfScens~=1
    figure
%     scatter(x_fromInverse, ones(1,length(samples)))
    scatter(samples, ones(1,length(samples)))
    xlabel('Wind power [W]')
end

%% Attempt 1 to obtain probabilities for each sample (not correct 14/03)

figure
% histForPDF = histogram(samples,length(samples),'Normalization','probability') %create histogram to obtain probability density values
histForPDF = histogram(samples,'Normalization','probability'); %create histogram to obtain probability density values

% finalScens = histForPDF.Data;
finalScens = samples;

for samp_ind = 1:length(samples)
    index1 = find(samples(samp_ind) < histForPDF.BinEdges,1,'first');
    probabilitiesOfFinalScens(samp_ind) = histForPDF.Values(index1-1);
end

%% Attempt 2 to obtain probabilities for each sample (not correct 14/03)

% x_sample = samples./(realizedPower*2);
% 
% %We use the analytical expression of the pdf, given the cdf above is: 
% for i = 1:length(x_sample)
% %     probabilitiesOfFinalScens(i) = -1 * b * (1 - x_sample(i))^(-1 - b) * (1/x_sample(i) - 1)^(-1 - b) / (x_sample(i)^2 * nthroot(1/x_sample(i) - 1, b + 2)^2);
% %     pdf = -1 * b * (1 - x).^(-1 - b) .* (1./x - 1).^(-1 - b) .* (x.^2 ./ (x - 1).^(b+2));
% %     pdf_unormalized(i) = (b / x_sample(i)^2) * (1 / ((1/x_sample(i) - 1)^((b+1)/b) + 1)) * (1 / b);
%     pdf_unorm(i) = b .* x_sample(i) .* ((1 - x_sample(i)).^((1/b) - 1)) ./ (x_sample(i).^(1 + (1/b))) .* (1./((1/x_sample(i) - 1).^(b+1)));
% end
% 
% % Integrate the unnormalized PDF over the domain
% pdf_integral = integral(@(x) pdf_unnormalized(x, 1), 0, 1);
% 
% % Define the parameters
% b = 2;
% vector1 = [0.1 0.2 0.3];
% x = 0:0.01:1;
% 
% % Evaluate the unnormalized PDF
% pdf_unnormalized = @(x, i) b * (1 - x).^(b-1) ./ vector1(i).^b;
% 
% % Integrate the unnormalized PDF over the domain
% pdf_integral = integral(@(x) pdf_unnormalized(x, 1), 0, 1);
% 
% % Define the normalized PDF as a function of x and i
% pdf_normalized = @(x, i) pdf_unnormalized(x, i) / pdf_integral;
% 
% % Evaluate the normalized PDF for the first value of vector1 at each x value
% pdf_values = pdf_normalized(x, 1);

% 
% probabilitiesOfFinalScens2 = pdf_value;

%% Attempt 3 to obtain probabilities for each sample (not correct 14/03)

% % Create a histogram with 10 bins
% [N, edges] = histcounts(samples, length(samples));
% 
% % Calculate the bin widths and centers
% bin_widths = diff(edges);
% bin_centers = edges(1:end-1) + bin_widths/2;
% 
% % Calculate the probabilities for each bin
% probabilities = N / sum(N);
% 
% % Plot the histogram with probabilities on the y-axis
% figure
% bar(bin_centers, probabilities, 'hist');
% ylabel('Probability');
% xlabel('Sample value');
