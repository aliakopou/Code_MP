function [finalScens,probabilitiesOfFinalScens] = getScenariosUsingInverseTransformSampling2(b,numberOfScens,realizedPower)

% b = 2;
% numberOfScens = 1000;
% realizedPower = 130*10^6;

%% Obtaining samples using 'Inverse Transform Sampling' method (https://en.wikipedia.org/wiki/Inverse_transform_sampling)

centerValueOfDistribution = realizedPower; % if we were to consider the distribution as a gaussian this would be the mean
% centerValueOfDistribution = 130*10^6; % if we were to consider the distribution as a gaussian this would be the mean 

rng(12)
vector1 = rand(1,numberOfScens); %sampling from Unif[0,1]

for i = 1:length(vector1)
    x_fromInverse(i) = 1/(nthroot((1/vector1(i) - 1),b)+1); %obtain normalized samples (wind power values) using the analytical expression of the inverse CDF function
end

samples = x_fromInverse*(centerValueOfDistribution*2); 
% samples = x_fromInverse; 

finalScens = samples;

probabilitiesOfFinalScens = [];

% histForPDF = histogram(samples,length(samples),'Normalization','probability') %create histogram to obtain probability density values
% 
% finalScens = histForPDF.Data;
% 
% probabilitiesOfFinalScens = histForPDF.Values;

%Plot the range of values
figure 
scatter(samples, ones(1,length(samples)))
xlabel('Wind power [W]')

