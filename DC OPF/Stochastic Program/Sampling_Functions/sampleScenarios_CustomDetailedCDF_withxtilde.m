function [finalScens,x_point,random_epsilon,current_sigma] = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled,useExistingxpoint,xalready1)

% b = 1;
% numOfScenarios = 100;
% % realizedPower = 130*10^6;
% y_value = 0.5;
% realization = 0.5;
% % x_point_init = 0.5; % Set the value for x_point_init
% lambda_DA = 60;
% seedBool = 1;
% UpperGenLimit = 2*150*10^6;

%% Obtain x_tilde = x0+epsilon by sampling epsilon from a truncated Gaussian (TODO)

% x0 = realization;
%
% mu = 0;      % mean of the Gaussian distribution
% sigma = 1;   % standard deviation of the Gaussian distribution
% lower = -2;  % lower truncation limit
% upper = 2;   % upper truncation limit
%
% % Create a truncated Gaussian distribution object
% trunc_gauss = makedist('TruncatedGaussian', 'mu', mu, 'sigma', sigma, 'lower', lower, 'upper', upper);
%
% % Generate samples from the truncated Gaussian distribution
% n_samples = 1000;
% samples = random(trunc_gauss, n_samples, 1);

%% Obtain x_tilde = x0+epsilon by sampling epsilon from a Gaussian

% x0 = realization;
linearRelationship = 1;

% payment = b;
if use_payment_scaled == 1
    max_payment = lambda_DA;
else
    max_payment = 2000;
end
payment_values = 1:max_payment; % payment value ranging from 1 to 2000
% min_sigma = 0.01;
min_sigma = 0;
% max_sigma = (0.9-0.01);
max_sigma = (0.1-0.01)/3; %since 99.7% of the data in a Gaussian distribution is within ±3 standard deviations from the mean, we set sigma value such that 3 times sigma equals 0.1

if linearRelationship==1
    % Define the function that maps payment to sigma (LINEAR)
    % sigma = max_sigma * (1 - payment / max_payment) + min_sigma;
    sigma = min_sigma + (max_sigma - min_sigma) * (1 - (payment_values - 1) / (max_payment-1));
else
    % Define the decay constant
    decay_constant = 5;
    % Define the NONLINEAR function that maps payment to sigma
    %     sigma = max_sigma * exp(-payment/decay_constant/max_payment) + min_sigma;
    %     sigma = max_sigma * exp(-payment_values * decay_constant / max_payment) + min_sigma;
    sigma = max_sigma * exp(-payment_values / (decay_constant * max_payment)) + min_sigma; %when payment is 1->sigma is at max, when payment is 2000->sigma is min
end

% Plot the relationship between payment and sigma
% figure
% plot(payment_values, sigma);
% xlabel('Payment');
% ylabel('Sigma');
% title('Payment vs. Sigma');

num_samples = 1000;
payment = b;
if linearRelationship==1
    %     current_sigma = max_sigma * (1 - payment / max_payment) + min_sigma;
    %     current_sigma = min_sigma + (max_sigma - min_sigma) * (1 - (payment_values - 1) / (max_payment-1));
    current_sigma = min_sigma + (max_sigma - min_sigma) * (1 - (payment - 1) / (max_payment-1));
else
    current_sigma = max_sigma * (1 - payment / max_payment) + min_sigma;
end
% current_sigma = sigma(payment); % find the corresponding sigma value for the chosen payment value
if seedBool == 1 %use a seed
%     rng(12)
    rng(randSeedNo)
end
epsilon_samples = normrnd(0, current_sigma, [num_samples, 1]);


% fig1 = figure
% %     scatter(x_fromInverse, ones(1,length(samples)))
% scatter(epsilon_samples, ones(1,length(epsilon_samples)))
% xlabel('epsilon (normalized)')

brk=0;
brk_it = 0;
%Randomly pick one epsilon value

% while brk==0

if seedBool == 1 %use a seed
%     rng(12)
    rng(randSeedNo)
end
random_sample_index = randi(num_samples);
random_epsilon = epsilon_samples(random_sample_index);
% itn=0;
% for i = 1:length(epsilon_samples)
% random_epsilon = epsilon_samples(i);
% itn = itn+1;
% if (realization  + random_epsilon)>=0 & (realization  + random_epsilon)<=1
%     break
% end
% end

% if realization - random_epsilon >0 & realization + random_epsilon <1
% brk = 1;
% brk_it = brk_it+1;
% end
% end

if useExistingxpoint==0
    % x_tilde = realization  + random_epsilon;
    x_tilde = realization  + random_epsilon*realization; %noise is function of the realization value
    % x_tilde = realization;
else
    x_tilde = xalready1;
end

%% Substitute x_tilde in custom CDF function

if use_payment_scaled == 1
    b_orig = b; % Set the value for b (range: 1 to lambda_DA)

    b_max = 2000;

    b_scaled = 1 + ((b_orig - 1)/(lambda_DA-1))*(b_max-1); %maps [1,2000(bmax)] to [1,lambda_DA]

else
    b_scaled = b;
end


% curvature_power = 3; % Set the value for curvature_power
% curvature_factor = (b_scaled/b_max)^curvature_power;
% curvature_factor = (b_orig/60)^curvature_power;
% x_point = x_point_init + (realization - x_point_init) * curvature_factor;

x_point=x_tilde;

% y_value = x_point;

% Compute value of g
g = -log((1/y_value - 1)^(1/b_scaled)+1)/log(x_point);

if seedBool == 1 %use a seed
%     rng(12)
    rng(randSeedNo)
end
vector1 = rand(1,numOfScenarios); %sampling from Unif[0,1]

for i = 1:length(vector1)
    x_fromInverse(i) = ( 1*(nthroot((1/vector1(i) - 1),b_scaled) +1 ))^(-1/g); %obtain normalized samples (wind power values) using the analytical expression of the inverse CDF function
end

% x_fromInverse_norm = x_fromInverse./((1/(nthroot((1/0.000001 - 1),b)+1))^(-1/g));

% fig2 = figure
% scatter(x_fromInverse,ones(1,length(x_fromInverse)))
% xlabel('Wind power (normalized)')

samples = x_fromInverse*(UpperGenLimit);
% samples = x_fromInverse;

finalScens = samples;

% Plot the range of values
% if numOfScenarios~=1
%     fig3 = figure
% %     scatter(x_fromInverse, ones(1,length(samples)))
%     scatter(samples, ones(1,length(samples)))
%     xlabel('Wind power [W]')
% end


%% Create one figure containing all previous figs as subplots

% % Create a new figure to hold the subplots
% fig_combined = figure;
%
% % Create the first subplot and copy the content from the first figure
% subplot(1, 3, 1); % 1 row, 3 columns, first plot
% copyobj(allchild(get(fig1, 'CurrentAxes')), gca);
%
% % Create the second subplot and copy the content from the second figure
% subplot(1, 3, 2); % 1 row, 3 columns, second plot
% copyobj(allchild(get(fig2, 'CurrentAxes')), gca);
%
% % Create the third subplot and copy the content from the third figure
% subplot(1, 3, 3); % 1 row, 3 columns, third plot
% copyobj(allchild(get(fig3, 'CurrentAxes')), gca);
%
% % Optional: Close the original figures if you don't need them anymore
% close(fig1);
% close(fig2);
% close(fig3);
