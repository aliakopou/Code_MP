function h = SamplingScenarios(mu,sigma,sz,numberOfBinsApproximation)

%     x = 0:0.01:1;
%     mu = 30;
%     sigma = 50;
%     sz = [1,100000];

    rng(12)

    samples = normrnd(mu,sigma,sz);
    % samples = betarnd(2,2);

%     figure
    hold on
    h = histogram(samples,numberOfBinsApproximation);
    h.Normalization = 'probability';

end