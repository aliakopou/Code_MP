function [finalScens,probabilitiesOfFinalScens] = getScenariosFromSimplifiedCDFfunction(b,numberOfScens,realizedPower)

%% CDF Analytical Expression

discretization = 1/numberOfScens;  %discretize based on the number of scenarios
range1 = 0:discretization:1;
x = range1; % set grid space


% b = 1;
% x_point = realizedPower/(realizedPower*2);
% y_value = 0.5;


% figure
% 
% % for b = 2
% % for b = [1,2,5,2000]
% % for b = 1:1:100
% 
% 
% y=@(x) 1/(1+(x^a/(1-x^a))^-(b));
% 
% fplot(y,[0 1])
% % hold on
% 
% % end
% 
% xlim([0 1])
% ylim([0 1])


%% Obtaining PDF from CDF

% l = 1;
% for x = range
%     te(l) = 1/(1+(x^a/(1-x^a))^-(b+1));
%     l=l+1;
% end

for i = 1:length(x)
    der(i) = (b*(-x(i)*(x(i)-1))^(b-1)) / (x(i)^b + (1-x(i))^b)^2;  % analytical expression for the derivative of analytical CDF function
end

por = der/(length(x)-1);

% por = diff(te);
figure
plot(x*(realizedPower*2*10^6),por)

%% Sampling Scenarios

% stream = RandStream('dsfmt19937','Seed',3); % set seed for datasample
% 
% randomSamples = datasample(stream,por, numberOfScens, 'Replace',false);
% 
% for l = 1:length(randomSamples)
%     IndexOfRandSamples(1,l) = find(por==randomSamples(l),1);
% end
% 
% finalScens = range(IndexOfRandSamples)*130*2*10^6;
% 
% finalScens(find(finalScens==0)) = [];  %remove any zero wind power value
% 
% probabilitiesOfFinalScens = randomSamples;
% 
% strcat('The sum of the scenario probabilities is 1 : ', {' '},string(sum(probabilitiesOfFinalScens) == 1)) %This should be true

finalScens = x*realizedPower*2*10^6; 

probabilitiesOfFinalScens = por;

tol1 = 1e-5; % set a tolerance value because of floating numbers

strcat('The sum of the scenario probabilities is equal to 1 : ', {' '}, string(abs(sum(probabilitiesOfFinalScens) - 1) < tol1)) %This should be true

indexOfZero = find(finalScens==0);

finalScens(indexOfZero) = [];  %remove wind power values equal to zero 
probabilitiesOfFinalScens(indexOfZero) = []; %also remove the corresponding probability



