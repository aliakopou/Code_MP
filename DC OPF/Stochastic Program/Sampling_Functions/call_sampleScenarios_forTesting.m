clear all
close all

%% Define Parameters

UpperGenLimit = 2*150*10^6;

y_value = 0.5;

% realizedPower = 150*10^6;

lambda_DA = 60;
% lambda_dayAheadRT = 100;

% b = lambda_DA;  %(range: 1 to lambda_DA)

% realization = realizedPower/UpperGenLimit;
realization = 0.8;
% x_point_init = 0.5; % Set the value for x_point_init (the median of the probability distribution (PDF) that the given forecast (CDF) represents)

numOfScenarios = 100;
seedBool = 0;

realizedPower = realization*UpperGenLimit;

%%

iterr = 1;
for b = linspace(1,30,10)
% for b = linspace(1,1,1)
%     close all
     [scens,x_point,random_epsilon]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);

%     [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);
% 
%     disp(['The mean of the forecast distribution for this b value is equal to ', num2str(x_point)])

    StochOptimization_revisited_singleTimestep

    crps = computingCRPS_usingAnalyticalFormula(5,lambda_DA,b,y_value,realization,x_point);

%     crps = computingCRPS_usingAnalyticalFormula(4,lambda_DA,b,y_value,realization,x_point_init);

    addit = 0;

    for itt = 1:numOfScenarios
        addit = addit + (1.*(  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/UpperGenLimit  -  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/UpperGenLimit  ));
    end

    totalPayment1(iterr) = sum( lambda_dayAhead(1,1)*GeneratorDayAheadPower/UpperGenLimit  + lambda_dayAhead(1,1)*WindDayAheadPower/UpperGenLimit  + (1/numOfScenarios)*addit );

    totalPayment(iterr) = totalPayment1(iterr) + (1-crps)*lambda_dayAhead(1,1);

    obj_it(iterr) = objectiveFunctionValue;

    crps_it(iterr) = crps;

    epsilon_it(iterr) = random_epsilon;

    b_it(iterr) = b;

    x_point_it(iterr) = x_point;

    iterr = iterr +1

end

figure
plot(b_it,totalPayment)

% figure
% plot((1-crps_it)*lambda_dayAhead(1,1),totalPayment)

figure
plot(b_it,obj_it+(1-crps_it).*b_it)

figure
plot(b_it,obj_it+(1-crps_it).*lambda_DA)



% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*b_it3(2:end))
% 
% figure
% plot(b_it3(2:end),obj_it3(2:end)+1.*b_it3(2:end))
% 
% 
% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*lambda_DA)

% figure
% plot(b_it,obj_it+linspace(1,2,10))