%%
% for mc_it=1:totIterNo
 
    %     iterr = 1;
 
%     realization = rangg(1);
% %     realization = rangg(mc_it);
%     realizedPower = realization*UpperGenLimit;
% 
    if seedBool == 1
        if useRandomSeed == 1
            randSeedNo = randi(10000);
        else
            randSeedNo = seedForIters(mc_it);
        end
    else 
        randSeedNo = [];
    end
% 
% 
%     Payment has to be given here, also the relationship payment b needs to be decided and kept constant, also the different optimization variables need to be set, to mc_it twra einai to itt, 
%     to ii den yparxei 
 
%%
    % for ii = 1:size(pay_concat,2)  %INSTEAD OF LOOP YOU GIVE VARIABLE "payment" AS INPUT HERE

        %             for ii = 10:10
        %     for b = linspace(1,10,100)
        %     for b = linspace(1,2,10)
        %     close all
        
        % if includeOptimization ==1
        %     payment = pay_concat(ii)
        % else
        %     payment = pay_concat(ii);
        % end

        mc_it = iterat;
        ii=1;
 
        %-- Define relationship between payment and b --%
        if relationship_payment_b == 1   %Linear realtionship
            b = payment;
        elseif relationship_payment_b == 2   %Exponential relationship
            min_payment = 1;
            max_payment = 2000;
            min_b = 1;
            max_b = 2000;
 
            %             base = 3; %the larger, the faster b increases
            curvature = 2.5;
 
            %         for ii = 1:1:2000
            % Normalize the payment value to the range [0, 1]
            %             normalized_payment = (pay_concat(ii) - min_payment) / (max_payment - min_payment);
            scaling_factor = (log(max_b / min_b)) / ((max_payment - min_payment)^curvature);
 
            % Apply the exponential relationship
            %             b = min_b + (1 - exp(-decay_constant * normalized_payment * (max_payment - min_payment))) * (max_b - min_b)
            b = min_b * exp(scaling_factor * (payment - min_payment)^curvature);
            %         end
 
            %          figure
            %          plot(1:length(b),b)
        elseif relationship_payment_b == 3 %Logarithmic relationship
            min_payment = 1;
            max_payment = 2000;
 
            min_b = 1;
            max_b = 2000;
 
            decay_constant = 5;
 
            % for ii = 1: 1:2000
            % Normalize payment to a range of [0, 1]
            normalized_payment = (payment - min_payment) / (max_payment - min_payment);
 
            % Logarithmic relationship between payment and b
            b = min_b + (log(1 + normalized_payment * (exp(decay_constant) - 1)) / log(exp(decay_constant))) * (max_b - min_b);
            % end
        else
            disp('Not Valid Relationship')
        end

 
        %-- Obtain the probabilistic wind forecast --%
        if useExistingxpoint==1
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint, xalready1(mc_it,ii));
            
            x_point = xalready1(mc_it,ii);
        else 
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint,[]);
        end
        %     [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);
        %
        %     disp(['The mean of the forecast distribution for this b value is equal to ', num2str(x_point)])
 
        %-- Run SOPF using the probabilistic wind forecast --%
        if includeOptimization == 1
            %             resultsSOPF = StochOptimization_revisited_singleTimestep(scens,realizedPower);
            StochOptimization_revisited_singleTimestep;
 
            resultsSOPF = resultsOpt;
 
            resultsSOPF.realization = realization;
 
            resultsSOPF.realizedPower = realizedPower;
 
            resultsSOPF.payment = payment;
 
            resultsSOPF.b = b;
            resultsSOPF.useb_scaled = use_payment_scaled;
 
            resultsAll(mc_it,ii) = resultsSOPF;
 
            lambda_DA = lambda_dayAhead(1,1);
 
            %             lambda_RT(:,:,mc_it,ii) = lambda_RT;
            %             clc
        end
 
 
        %-- Compute crps value --%
        crps = computingCRPS_usingAnalyticalFormula(5, lambda_DA, b, y_value, realization, x_point, plotCDF, use_payment_scaled);
 
        %     crps = computingCRPS_usingAnalyticalFormula(4,lambda_DA,b,y_value,realization,x_point_init);
 
 
        if includeOptimization == 1
            addit = 0;
 
            for itt = 1:numOfScenarios
                addit = addit + (1.*(  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/UpperGenLimit  -  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/UpperGenLimit  ));
            end
 
            totalPayment1(mc_it,ii) = sum( lambda_dayAhead(1,1)*GeneratorDayAheadPower/UpperGenLimit  + lambda_dayAhead(1,1)*WindDayAheadPower/UpperGenLimit  + (1/numOfScenarios)*addit );
 
            totalPayment(mc_it,ii) = totalPayment1(mc_it,ii) + (1-crps)*lambda_dayAhead(1,1);
 
 
            obj_it(mc_it,ii) = objectiveFunctionValue;
 
        end
 
        crps_it(mc_it,ii) = crps;
 
        epsilon_sample_it(mc_it,ii) = random_epsilon;
        
        epsilon_it(mc_it,ii) = random_epsilon*realization;
 
        b_it(mc_it,ii) = b;
        
        if useExistingxpoint==0
            x_point_it(mc_it,ii) = x_point;
        end
 
        sigma_it(mc_it,ii) = current_sigma;
        
        if seedBool == 1
            randSeedNo_it(mc_it,ii) = randSeedNo;
        end
 
 
        %     close all
 
        if includeOptimization ==1
            % mc_it %monte carlo iteration number
            iterat
        end
 
        %         iterr = iterr +1;
 
    % end

    %%
 
%     if includeOptimization ==0
%         mc_it %monte carlo iteration number
%     end
% 
% 
% % end
% 
% iterationsTotalTime = toc;
% 
% 
% payment_it = pay_concat;  %Values in range: [1 to 2000]
% 
% if use_payment_scaled == 0
% 
%     %     it23 = 1;
%     %     for it23 = 1: length(btemp)
%     %     for b = 1:1:20
%     %     pay_scaled = pay_temp;
%     pay_max = 2000;
%     pay_scaledInLambda = ((payment_it - 1) / (pay_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
% 
%     b_max = 2000;
%     b_scaledInLambda = ((b_it - 1) / (b_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
%     %         it23 = it23+1;
%     %     end
% end
% 
% 
% b_it_unchanged = b_it;