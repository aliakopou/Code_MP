clear all
% clearvars -except xpoint_mean crps_mean
close all

%% Define Constants

UpperGenLimit = 2*150*10^6;

y_value = 0.5;

% realizedPower = 150*10^6;

lambda_DA = 60;
% lambda_dayAheadRT = 100;

% b = lambda_DA;  %(range: 1 to lambda_DA)

% realization = realizedPower/UpperGenLimit;
% realization = 0.7;
% x_point_init = 0.5; % Set the value for x_point_init (the median of the probability distribution (PDF) that the given forecast (CDF) represents)

% realizedPower = realization*UpperGenLimit;

numOfScenarios = 100;

%% Set code parameters
seedBool = 1;

useRandomSeed = 0;

use_payment_scaled = 0; %if set to 1: give payment values between 1 and lambda

includeOptimization = 1; %if set equal to 1 then SOPF problem is solved

plotCDF=1;

relationship_payment_b = 2;

if relationship_payment_b == 2
    curvature = 1;
elseif relationship_payment_b == 3
    decay_constant = 5;
end

useExistingxpoint = 0;

includeRealizationInScenarios = 0;

%% Define parameter sweep values

%-- Payment --%
if relationship_payment_b == 3  %Logarithmic relationship & decay = 5
    payRange1 = 1:1:50;
    %         btemp2 = [100 500 1000 1500 2000];
    payRange2 = 51:10:100;
    %     btemp2 = 100;
    payRange3 = 101:250:1500;
    payRange4 = 1500:250:2000;
elseif relationship_payment_b == 2 %Exponential relationship
    payRange1 = 1:1:50;
    payRange2 = 51:1:1000;
    payRange3 = 1001:1:1320;
    payRange4 = 2000:1:2000;
elseif relationship_payment_b == 1 %Linear relationship
    payRange1 = 1:10:21;
    payRange2 = 22:1:80;
    payRange3 = 81:10:100;
    payRange4 = 500:500:2000;
else
    disp('Not Valid Relationship')
end

pay_concat = [1,100,2000];  %the payment values
% pay_concat = 100;
% pay_concat = (1:10:2000);
% pay_concat = (1:1:2000);
% pay_concat = (1:100:2000);

%-- Realization --%
% rangg = [0.2,0.4,0.6,0.8];
rangg = 0.4;


%% Load data
if useExistingxpoint==1
    xalready1 = xpoint_mean;
else
    xalready1 = [];
end

%%

tic

totIterNo = 2;

load('seedForIters')

% seedForIters = randi(10000,[1,totIterNo]);
% seedForIters = [5782, 8540, 681, 4646];

%mc_it = 1; %monte carlo iteration number
%for mc_it=1:size(rangg,2) %monte carlo iterations
for mc_it=2:totIterNo

    %     iterr = 1;

    realization = rangg(1);
    %     realization = rangg(mc_it);
    realizedPower = realization*UpperGenLimit;

    if seedBool == 1
        if useRandomSeed == 1
            randSeedNo = randi(10000);
        else
            randSeedNo = seedForIters(mc_it);
        end
    else
        randSeedNo = [];
    end

    for ii = 1:size(pay_concat,2)
        %             for ii = 10:10
        %     for b = linspace(1,10,100)
        %     for b = linspace(1,2,10)
        %     close all

        if includeOptimization ==1
            payment = pay_concat(ii)
        else
            payment = pay_concat(ii);
        end

        %-- Define relationship between payment and b --%
        if relationship_payment_b == 1   %Linear realtionship
            b = payment;
        elseif relationship_payment_b == 2   %Exponential relationship
            min_payment = 1;
            max_payment = 2000;
            min_b = 1;
            max_b = 2000;

            %             base = 3; %the larger, the faster b increases
            

            %         for ii = 1:1:2000
            % Normalize the payment value to the range [0, 1]
            %             normalized_payment = (pay_concat(ii) - min_payment) / (max_payment - min_payment);
            scaling_factor = (log(max_b / min_b)) / ((max_payment - min_payment)^curvature);

            % Apply the exponential relationship
            %             b = min_b + (1 - exp(-decay_constant * normalized_payment * (max_payment - min_payment))) * (max_b - min_b)
            b = min_b * exp(scaling_factor * (payment - min_payment)^curvature);
            %         end

            %          figure
            %          plot(1:length(b),b)
        elseif relationship_payment_b == 3 %Logarithmic relationship
            min_payment = 1;
            max_payment = 2000;

            min_b = 1;
            max_b = 2000;

            

            % for ii = 1: 1:2000
            % Normalize payment to a range of [0, 1]
            normalized_payment = (payment - min_payment) / (max_payment - min_payment);

            % Logarithmic relationship between payment and b
            b = min_b + (log(1 + normalized_payment * (exp(decay_constant) - 1)) / log(exp(decay_constant))) * (max_b - min_b);
            % end
        else
            disp('Not Valid Relationship')
        end


        %-- Obtain the probabilistic wind forecast --%
        if useExistingxpoint==1
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint, xalready1(mc_it,ii));

            x_point = xalready1(mc_it,ii);
        else
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint,[]);
        end
        %     [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);
        %
        %     disp(['The mean of the forecast distribution for this b value is equal to ', num2str(x_point)])

        %-- Run SOPF using the probabilistic wind forecast --%
        if includeOptimization == 1
            %             resultsSOPF = StochOptimization_revisited_singleTimestep(scens,realizedPower);
            StochOptimization_revisited_singleTimestep;

            resultsSOPF = resultsOpt;

            resultsSOPF.realization = realization;

            resultsSOPF.realizedPower = realizedPower;

            resultsSOPF.pay_concat = pay_concat;

            resultsSOPF.b = b;
            resultsSOPF.useb_scaled = use_payment_scaled;

            resultsAll(mc_it,ii) = resultsSOPF;

            lambda_DA = lambda_dayAhead(1,1);

            %             lambda_RT(:,:,mc_it,ii) = lambda_RT;
            %             clc
        end


        %-- Compute crps value --%
        crps = computingCRPS_usingAnalyticalFormula(5, lambda_DA, b, y_value, realization, x_point, plotCDF, use_payment_scaled);

        %     crps = computingCRPS_usingAnalyticalFormula(4,lambda_DA,b,y_value,realization,x_point_init);


        if includeOptimization == 1
            addit = 0;

            for itt = 1:numOfScenarios
                addit = addit + (1.*(  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/UpperGenLimit  -  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/UpperGenLimit  ));
            end

            totalPayment1(mc_it,ii) = sum( lambda_dayAhead(1,1)*GeneratorDayAheadPower/UpperGenLimit  + lambda_dayAhead(1,1)*WindDayAheadPower/UpperGenLimit  + (1/numOfScenarios)*addit );

            totalPayment(mc_it,ii) = totalPayment1(mc_it,ii) + (1-crps)*lambda_dayAhead(1,1);


            obj_it(mc_it,ii) = objectiveFunctionValue;

        end

        crps_it(mc_it,ii) = crps;

        epsilon_sample_it(mc_it,ii) = random_epsilon;

        epsilon_it(mc_it,ii) = random_epsilon*realization;

        b_it(mc_it,ii) = b;

        if useExistingxpoint==0
            x_point_it(mc_it,ii) = x_point;
        end

        sigma_it(mc_it,ii) = current_sigma;

        if seedBool == 1
            randSeedNo_it(mc_it,ii) = randSeedNo;
        end


        %     close all

        if includeOptimization ==1
            mc_it %monte carlo iteration number
        end

        %         iterr = iterr +1;

    end

    if includeOptimization ==0
        mc_it %monte carlo iteration number
    end


end

iterationsTotalTime = toc;


payment_it = pay_concat;  %Values in range: [1 to 2000]

if use_payment_scaled == 0

    %     it23 = 1;
    %     for it23 = 1: length(btemp)
    %     for b = 1:1:20
    %     pay_scaled = pay_temp;
    pay_max = 2000;
    pay_scaledInLambda = ((payment_it - 1) / (pay_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
    % pay_scaledInLambda = ((payment_it - 1) / (pay_max - 1)) * lambda_DA; % Value in range: [1 to lambda_DA]

    b_max = 2000;
    b_scaledInLambda = ((b_it - 1) / (b_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
    %         it23 = it23+1;
    %     end
end


b_it_unchanged = b_it;


% b_it = [1 1.0295 1.0590 1.0885 1.1181 1.1476 1.1771 1.2066 1.2361 1.2656 1.2951 1.3247 1.3542 1.3837 1.4132 1.4427 1.4722 1.5018 1.5313 1.5608];


%% Calculate Revenue of Wind Farm

if includeOptimization == 1
    for i = 1:size(resultsAll,1)
        for pp = 1:length(pay_scaledInLambda)
            revenueOfWindFarm(i,pp) = lambda_dayAhead(1,1)*(resultsAll(i,pp).WindDayAheadPower_pu*resultsAll(i,pp).BasePower)/UpperGenLimit + (1-crps_it(i,pp)).*pay_scaledInLambda(1,pp);
        end
    end

    figure
    plot(payment_it,revenueOfWindFarm)
    ylabel('Revenue of Wind Farm')

end

%% Plotting


figure
% hold on
plot(payment_it,b_it) %values in [1 to 2000]
xlabel('payment');
ylabel('b');

figure
% hold on
plot(pay_scaledInLambda,b_scaledInLambda) %values in [1 to lambda]
xlabel('payment in lambda');
ylabel('b in lambda');


figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),crps_it(i,:))
    plot(pay_scaledInLambda(1,:),crps_it(i,:))
    hold on
end
ylabel('crps');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),crps_it(i,:))
    plot(payment_it(1,:),crps_it(i,:))
    hold on
end
ylabel('crps');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),x_point_it(i,:))
    plot(pay_scaledInLambda(1,:),x_point_it(i,:))
    hold on
end
ylabel('x\_point');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),x_point_it(i,:))
    plot(payment_it(1,:),x_point_it(i,:))
    hold on
end
ylabel('x\_point');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),(1-crps_it(i,:)).*b_it(i,:))
    plot(pay_scaledInLambda(1,:),(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
    hold on
end
ylabel('(1-crps).*pay\_scaledInLambda');


if includeOptimization == 1
    figure
    for i=1:size(b_it,1)
%     for i=1:10
        %         plot(b_it(i,:),obj_it(i,:))
        plot(pay_scaledInLambda(1,:),obj_it(i,:))
        hold on
    end
    ylabel('objective');

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),totalPayment(i,:))
        plot(pay_scaledInLambda(1,:),totalPayment(i,:))
        hold on
    end
    ylabel('totalPayment');

    % figure
    % plot((1-crps_it)*lambda_dayAhead(1,1),totalPayment)

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        % plot(pay_scaledInLambda(1,:),obj_it(i,:)+(1-crps_it(i,:)).*(pay_scaledInLambda(1,:)/2))
        plot(pay_scaledInLambda(1,:),obj_it(i,:)+(1-crps_it(i,:)).*(pay_scaledInLambda(1,:)*realization))
        hold on
    end
    ylabel('Total System Cost [CHF]');
    xlabel('Payment [CHF]');

    figure
    for i=1:size(b_it,1)
        % for i=1:10
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*(pay_scaledInLambda(1,:)*realization))
        % plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*(pay_scaledInLambda(1,:)/2)*1)
        % plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*(pay_scaledInLambda(1,:)/1)*1)
        hold on
    end
    ylabel('Total System Cost [CHF]');

    %%

    figure
    for i=1:size(b_it,1)
        %             plot(pay_concat(i,:),obj_it(i,:)+(1-crps_it(i,:)).*pay_concat(i,:))
        plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*payment_it(1,:))
        hold on
    end
    ylabel('obj+(1-crps).*payment');


    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),totalPayment1(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        plot(pay_scaledInLambda(1,:),totalPayment1(i,:)+(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
        hold on
    end
    ylabel('totalPayment1+(1-crps).*pay\_scaledInLambda');

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*lambda_DA)
        plot(pay_scaledInLambda(1,:),obj_it(i,:)+(1-crps_it(i,:)).*lambda_DA)
        hold on
    end
    ylabel('obj+(1-crps).*lambda\_DA');



    bb = totalPayment1(i,:)+(1-crps_it(i,:)).*b_it(i,:);
    min1 = find(bb== min(bb))

    min2 = find(totalPayment==min(totalPayment))

end
% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*b_it3(2:end))
%
% figure
% plot(b_it3(2:end),obj_it3(2:end)+1.*b_it3(2:end))
%
%
% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*lambda_DA)

% figure
% plot(b_it,obj_it+linspace(1,2,10))


%%

if includeOptimization == 1

    if size(resultsAll,1)>1

        figure
        %     for i1 = 1:size(resultsAll,1)
        for i1 = 1:1
            for i=1:size(pay_scaledInLambda,2)
                %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
                objtemp1(1,i) = resultsAll(i1,i).objective;
                %             plot(pay_scaledInLambda(1,i),resultsAll(i1,i).objective+(1-crps_it(i1,i)).*pay_scaledInLambda(1,i))
                max1 = max(objtemp1);
            end
            plot(pay_scaledInLambda(1,:),objtemp1(1,:)+(1-crps_it(i1,:)).*pay_scaledInLambda(1,:))
            hold on
        end

        figure
        %         for i1 = 1:size(resultsAll,1)
        for i1 = 2:2
            plot(pay_scaledInLambda(1,:),totalPayment(i1,:))
            %         hold on
        end


    end

end


%% Monte Carlo estimated statistics

epsilon_mean = mean(epsilon_it);
figure
plot(pay_scaledInLambda,epsilon_mean)
ylabel('mean epsilon');

xpoint_mean = mean(x_point_it);
figure
plot(pay_scaledInLambda,xpoint_mean)
ylabel('mean x\_point');

crps_mean = mean(crps_it);
figure
plot(payment_it,crps_mean)
ylabel('mean crps');

if includeOptimization == 1

    figure
    plot(pay_scaledInLambda(1,:),mean(obj_it(:,:))+(1-mean(crps_it(:,:))).*pay_scaledInLambda(1,:))

    totMeanFunc = mean(obj_it(:,:))+(1-mean(crps_it(:,:))).*pay_scaledInLambda(1,:);
    figure
    plot(payment_it(1,:),totMeanFunc)

    incr1 = 1;
    for tempV = 10:10:size(obj_it,1)
        % for tempV = size(obj_it,1):10:size(obj_it,1)
        clear minn minIndex tempp
        % for ii = 1:size(obj_it,1)
        for ii = 1:tempV
            tempp = obj_it(ii,:)+(1-crps_it(ii,:)).*pay_scaledInLambda(1,:);
            temppAll(ii,:) = tempp;
            [minn(ii,:),minIndex(ii,:)] = min(tempp);
        end

        minn(:,:)
        optPayment = payment_it(minIndex)

        meanP(incr1) = mean(optPayment)  % optimal mean payment value as number of MC increases
        % mean(minIndex(:,1))

        incr1 = incr1 +1;
    end

    %Plot the mean opt payment for an increasing number of iterations
    figure
    plot((1:length(meanP)).*10,meanP)

    %--Plot the mean opt payment for an increasing number of iterations but for a different order (shuffling the marix first)
    tempM1 = optPayment.';

    for tt = 1:100
        shuffled_pay = tempM1(randperm(size(tempM1, 1)), :);
        for N = 10:10:200
            mean_values(N/10) = mean(shuffled_pay(1:N, :));
        end

        if tt==1
            figure
        else
            hold on
            plot(10:10:200, mean_values, '-o');
            xlabel('Number of Elements (N)');
            ylabel('Mean');
        end
    end

    %--Plot the mean opt payment for an increasing number of iterations but
    %for a different order (shuffling the marix first) for 400 (larger size than the matrix)

    for tt = 1:100
        tempM2 = [tempM1; tempM1(randperm(size(tempM1, 1)))];

        shuffled_tempM2 = tempM2(randperm(size(tempM2, 1)));

        for N = 10:10:400
            mean_valuesM2(N/10) = mean(shuffled_tempM2(1:N)); %calculate the mean for the first N elements (N = 10, 20, 30, ..., 400)
        end
        if tt==1
            figure
        else
            hold on
        end
        plot(10:10:400, mean_valuesM2, '-o');
        xlabel('Number of Elements (N)');
        ylabel('Mean');
    end


end

%--Plotting percentage increase for all other payment values --%%

%--For the mean:
for i = 1:1
    [lowestValueMean,lowestIndexMean] = min(totMeanFunc);
    percentIncreaseMean(1,:) = 1 * (totMeanFunc(1,:) - lowestValueMean) ./ lowestValueMean;
end

figure
for i = 1:size(percentIncreaseMean,1)
    plot(pay_concat,percentIncreaseMean(i,:))
    hold on
end
ylim([0 (0.01/24)])


%--For all iterations:
% for i = 1:size(temppAll,1)
% for i = 1:10
%     lowestValue = minn(i,:);
%     percentIncrease(i,:) = 1 * (temppAll(i,:) - lowestValue) ./ lowestValue;
% end
%
% figure
% for i = 1:size(percentIncrease,1)
% plot(pay_concat,percentIncrease(i,:))
% hold on
% end
% ylim([0 0.01])

%% Count number of times each value is picked

uniqueVals = unique(optPayment);

for i=1:length(uniqueVals)
    valT = uniqueVals(i);
    countOccurences(1,valT) = sum(optPayment==valT);
end

valuePickedMostTimes = find(countOccurences==max(countOccurences))

%% Analyzing ResultsAll

s1 = size(resultsAll,1); %number of MC iterations
s2 = size(resultsAll,2); %number of payment values

%Lambda
for i = 1:s1
    for k = 1:s2
        lDA(i,k) = resultsAll(i,k).lambda_dayAhead(1,1); %finding lambda Day Ahead Values
    end
end

mean(lDA)

%Regulation
for i = 1:s1
    for k = 1:s2
        SumUpRegulation(i,k) = sum(resultsAll(i,k).GeneratorUpPower_pu);
        SumDownRegulation(i,k) = sum(resultsAll(i,k).GeneratorDownPower_pu);
    end
end

meanUpReg = mean(SumUpRegulation);
meanDownReg = mean(SumDownRegulation);

totalReg = meanUpReg+meanDownReg;

figure
plot(payment_it,meanUpReg)
hold on
plot(payment_it,meanDownReg)

figure
plot(payment_it,totalReg)

%Day Ahead commited
for i = 1:s1
    for k = 1:s2
        GenPowerDA(i,k) = resultsAll(i,k).GeneratorDayAheadPower_pu;
        WPowerDA(i,k) = resultsAll(i,k).WindDayAheadPower_pu;
    end
end

mean_GenPowerDA = mean(GenPowerDA);
mean_WPowerDA = mean(WPowerDA);

figure
plot(payment_it,mean_GenPowerDA)

figure
plot(payment_it,mean_WPowerDA)

%%

%
% %Mean
% mean_value1 = mean(crps_it);
% mean_value2 = mean(x_point_it);
% mean_value3 = mean((1-crps_it).*b_it);
%
% figure
% plot(b_it,mean_value1)
%
% figure
% plot(b_it,mean_value2)
%
% figure
% plot(b_it,mean_value3)
%
% %Median
% median_value1 = median(crps_it);
% median_value2 = median(x_point_it);
% median_value3 = median((1-crps_it).*b_it);
%
% figure
% plot(b_it,median_value1)
%
% figure
% plot(b_it,median_value2)
%
% figure
% plot(b_it,median_value3)
%
% %Variance
% variance_value1 = var(crps_it);
% variance_value2 = var(x_point_it);
% variance_value3 = var((1-crps_it).*b_it);
%
% figure
% plot(b_it,variance_value1)
%
% figure
% plot(b_it,variance_value2)
%
% figure
% plot(b_it,variance_value3)
%
% %Standard deviation
% std_deviation1 = std(crps_it);
% std_deviation2 = std(x_point_it);
% std_deviation3 = std((1-crps_it).*b_it);
%
% figure
% plot(b_it,std_deviation1)
%
% figure
% plot(b_it,std_deviation2)
%
% figure
% plot(b_it,std_deviation3)
%
%
% %Confidence intervals
% confidence_level = 0.95; % For a 95% confidence interval
% alpha = 1 - confidence_level;
% lower_bound = quantile(crps_it, alpha/2);
% upper_bound = quantile(crps_it, 1 - alpha/2);
%
% %Probability density function (histogram):
% num_bins = 50; % Adjust this value based on your data
% figure;
% histogram(crps_it, num_bins, 'Normalization', 'pdf');
%
% %Cumulative distribution function (empirical CDF)
% cdfplot(crps_it);


