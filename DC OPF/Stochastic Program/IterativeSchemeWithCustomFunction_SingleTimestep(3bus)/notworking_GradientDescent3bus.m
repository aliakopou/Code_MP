clear all
% clearvars -except xpoint_mean crps_mean
close all

%% Define Constants

UpperGenLimit = 2*150*10^6;

y_value = 0.5;

% realizedPower = 150*10^6;

lambda_DA = 60;
% lambda_dayAheadRT = 100;

% b = lambda_DA;  %(range: 1 to lambda_DA)

% realization = realizedPower/UpperGenLimit;
% realization = 0.7;
% x_point_init = 0.5; % Set the value for x_point_init (the median of the probability distribution (PDF) that the given forecast (CDF) represents)

% realizedPower = realization*UpperGenLimit;

numOfScenarios = 100;

stopCriterion = 0;
initPayment = 1;
payment_GD(1) = initPayment;
payment_GD(2) = 1 + 0.0015;
% ref = 33.2332406700932;
ref = 33.70;

%% Set code parameters
seedBool = 1;

useRandomSeed = 0;

use_payment_scaled = 0; %if set to 1: give payment values between 1 and lambda

includeOptimization = 1; %if set equal to 1 then SOPF problem is solved

plotCDF=0;

relationship_payment_b = 1;

useExistingxpoint = 0;

includeRealizationInScenarios = 0;

%% Load data
if useExistingxpoint==1
    xalready1 = xpoint_mean;
else
    xalready1 = [];
end

%% Define parameter sweep values

%-- Payment --%
if relationship_payment_b == 3  %Logarithmic relationship & decay = 5
    payRange1 = 1:1:50;
    %         btemp2 = [100 500 1000 1500 2000];
    payRange2 = 51:100:1000;
    %     btemp2 = 100;
    payRange3 = 1100:250:1500;
    payRange4 = 1500:250:2000;
elseif relationship_payment_b == 2 %Exponential relationship
    payRange1 = 1:100:50;
    payRange2 = 50:100:1000;
    payRange3 = 1160:100:1220;
    payRange4 = 1220:100:2000;
elseif relationship_payment_b == 1 %Linear relationship
    payRange1 = 1:10:21;
    payRange2 = 22:1:80;
    payRange3 = 81:10:100;
    payRange4 = 500:500:2000;
else
    disp('Not Valid Relationship')
end

pay_concat = cat(2,payRange1,payRange2,payRange3,payRange4);  %the payment values
% pay_concat = 100;
% pay_concat = (1:10:2000);
% pay_concat = (1:1:2000);
% pay_concat = (1:100:2000);

%-- Realization --%
% rangg = [0.2,0.4,0.6,0.8];
rangg = 0.2;



%%

tic

totIterNo = 1;

load('seedForIters')

% seedForIters = randi(10000,[1,totIterNo]);
% seedForIters = [5782, 8540, 681, 4646];

% mc_it = 1; %monte carlo iteration number
% for mc_it=1:size(rangg,2) %monte carlo iterations
for mc_it=1:totIterNo

    %     iterr = 1;

    realization = rangg(1);
    %     realization = rangg(mc_it);
    realizedPower = realization*UpperGenLimit;

    if seedBool == 1
        if useRandomSeed == 1
            randSeedNo = randi(10000);
        else
            randSeedNo = seedForIters(mc_it);
        end
    else
        randSeedNo = [];
    end




    ii = 1;

    % for ii = 1:size(pay_concat,2)
    while stopCriterion==0
        %             for ii = 10:10
        %     for b = linspace(1,10,100)
        %     for b = linspace(1,2,10)
        %     close all


        alpha = 1; %learning rate
        
        if ii>=3
            der(ii) = ( ((obj_it(mc_it,ii-2)+ (1-crps_it(1,ii-2))*payment_GD(1,ii-2))-ref) - ((obj_it(mc_it,ii-1)+ (1-crps_it(1,ii-1))*payment_GD(1,ii-1))-ref) ) / (  (payment_GD(ii-2)) - ( (payment_GD(ii-1))) );
            
            payment_GD(ii) =  (payment_GD(ii-1)) - alpha*( ((obj_it(mc_it,ii-1)+ (1-crps_it(1,ii-1))*payment_GD(1,ii-1) )-ref) /der(ii))
        end



        % if includeOptimization ==1
        %     payment = payment_GD(ii)
        % else
        %     payment = payment_GD(ii);
        % end


        if use_payment_scaled == 0
            pay_max = 2000;
            pay_scaled = payment_GD(ii); % Scaled value in the range [1, lambda_DA]
            payment = ((pay_scaled - 1) / (lambda_DA - 1)) * (pay_max - 1) + 1

            %     it23 = 1;
            %     for it23 = 1: length(btemp)
            %     for b = 1:1:20
            %     pay_scaled = pay_temp;
            % pay_max = 2000;
            % pay_scaledInLambda(1,ii) = ((payment - 1) / (pay_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
            % 
            % b_max = 2000;
            % b_scaledInLambda(1,ii) = ((b_it - 1) / (b_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]
            %         it23 = it23+1;
            %     end
        end



        %-- Define relationship between payment and b --%
        if relationship_payment_b == 1   %Linear realtionship
            b = payment;
        elseif relationship_payment_b == 2   %Exponential relationship
            min_payment = 1;
            max_payment = 2000;
            min_b = 1;
            max_b = 2000;

            %             base = 3; %the larger, the faster b increases
            curvature = 3;

            %         for ii = 1:1:2000
            % Normalize the payment value to the range [0, 1]
            %             normalized_payment = (pay_concat(ii) - min_payment) / (max_payment - min_payment);
            scaling_factor = (log(max_b / min_b)) / ((max_payment - min_payment)^curvature);

            % Apply the exponential relationship
            %             b = min_b + (1 - exp(-decay_constant * normalized_payment * (max_payment - min_payment))) * (max_b - min_b)
            b = min_b * exp(scaling_factor * (payment - min_payment)^curvature);
            %         end

            %          figure
            %          plot(1:length(b),b)
        elseif relationship_payment_b == 3 %Logarithmic relationship
            min_payment = 1;
            max_payment = 2000;

            min_b = 1;
            max_b = 2000;

            decay_constant = 5;

            % for ii = 1: 1:2000
            % Normalize payment to a range of [0, 1]
            normalized_payment = (payment - min_payment) / (max_payment - min_payment);

            % Logarithmic relationship between payment and b
            b = min_b + (log(1 + normalized_payment * (exp(decay_constant) - 1)) / log(exp(decay_constant))) * (max_b - min_b);
            % end
        else
            disp('Not Valid Relationship')
        end


        %-- Obtain the probabilistic wind forecast --%
        if useExistingxpoint==1
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint, xalready1(mc_it,ii));

            x_point = xalready1(mc_it,ii);
        else
            [scens,x_point,random_epsilon,current_sigma]  = sampleScenarios_CustomDetailedCDF_withxtilde(b, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool, randSeedNo, use_payment_scaled, useExistingxpoint,[]);
        end
        %     [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numOfScenarios, UpperGenLimit, seedBool);
        %
        %     disp(['The mean of the forecast distribution for this b value is equal to ', num2str(x_point)])

        %-- Run SOPF using the probabilistic wind forecast --%
        if includeOptimization == 1
            %             resultsSOPF = StochOptimization_revisited_singleTimestep(scens,realizedPower);
            StochOptimization_revisited_singleTimestep;

            resultsSOPF = resultsOpt;

            resultsSOPF.realization = realization;

            resultsSOPF.realizedPower = realizedPower;

            resultsSOPF.pay_concat = pay_concat;

            resultsSOPF.b = b;
            resultsSOPF.useb_scaled = use_payment_scaled;

            resultsAll(mc_it,ii) = resultsSOPF;

            lambda_DA = lambda_dayAhead(1,1);

            %             lambda_RT(:,:,mc_it,ii) = lambda_RT;
            %             clc
        end


        %-- Compute crps value --%
        crps = computingCRPS_usingAnalyticalFormula(5, lambda_DA, b, y_value, realization, x_point, plotCDF, use_payment_scaled);

        %     crps = computingCRPS_usingAnalyticalFormula(4,lambda_DA,b,y_value,realization,x_point_init);


        if includeOptimization == 1
            addit = 0;

            for itt = 1:numOfScenarios
                addit = addit + (1.*(  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/UpperGenLimit  -  lambda_RT(itt,1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/UpperGenLimit  ));
            end

            totalPayment1(mc_it,ii) = sum( lambda_dayAhead(1,1)*GeneratorDayAheadPower/UpperGenLimit  + lambda_dayAhead(1,1)*WindDayAheadPower/UpperGenLimit  + (1/numOfScenarios)*addit );

            totalPayment(mc_it,ii) = totalPayment1(mc_it,ii) + (1-crps)*lambda_dayAhead(1,1);


            obj_it(mc_it,ii) = objectiveFunctionValue;

        end

        crps_it(mc_it,ii) = crps;

        epsilon_sample_it(mc_it,ii) = random_epsilon;

        epsilon_it(mc_it,ii) = random_epsilon*realization;

        b_it(mc_it,ii) = b;

        if useExistingxpoint==0
            x_point_it(mc_it,ii) = x_point;
        end

        sigma_it(mc_it,ii) = current_sigma;

        if seedBool == 1
            randSeedNo_it(mc_it,ii) = randSeedNo;
        end


        %     close all

        if includeOptimization ==1
            mc_it %monte carlo iteration number
        end

        %         iterr = iterr +1;

        if ii>3
            if ( ( ( obj_it(mc_it,ii-1)+ (1-crps_it(1,ii-1))*payment_GD(1,ii-1) )-ref ) - ( ( obj_it(mc_it,ii)+ (1-crps_it(1,ii))*payment_GD(1,ii) )-ref ) )<=0.0005 && ( ((obj_it(mc_it,ii-1)+ (1-crps_it(1,ii-1))*payment_GD(1,ii-1))-ref)-((obj_it(mc_it,ii)+ (1-crps_it(1,ii))*payment_GD(1,ii))-ref) )>=0
                stopCriterion =1;
            end
        end

        ii = ii+1;

    end










    if includeOptimization ==0
        mc_it %monte carlo iteration number
    end


end

iterationsTotalTime = toc;


payment_it = pay_concat;  %Values in range: [1 to 2000]




b_it_unchanged = b_it;


% b_it = [1 1.0295 1.0590 1.0885 1.1181 1.1476 1.1771 1.2066 1.2361 1.2656 1.2951 1.3247 1.3542 1.3837 1.4132 1.4427 1.4722 1.5018 1.5313 1.5608];


%% Calculate Revenue of Wind Farm

if includeOptimization == 1
    for i = 1:size(resultsAll,1)
        for pp = 1:length(pay_scaledInLambda)
            revenueOfWindFarm(i,pp) = lambda_dayAhead(1,1)*(resultsAll(i,pp).WindDayAheadPower_pu*resultsAll(i,pp).BasePower)/UpperGenLimit + (1-crps_it(i,pp)).*pay_scaledInLambda(1,pp);
        end
    end

    figure
    plot(payment_it,revenueOfWindFarm)
    ylabel('Revenue of Wind Farm')

end

%% Plotting


figure
% hold on
plot(payment_it,b_it) %values in [1 to 2000]
xlabel('payment');
ylabel('b');

figure
% hold on
plot(pay_scaledInLambda,b_scaledInLambda) %values in [1 to lambda]
xlabel('payment in lambda');
ylabel('b in lambda');


figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),crps_it(i,:))
    plot(pay_scaledInLambda(1,:),crps_it(i,:))
    hold on
end
ylabel('crps');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),crps_it(i,:))
    plot(payment_it(1,:),crps_it(i,:))
    hold on
end
ylabel('crps');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),x_point_it(i,:))
    plot(pay_scaledInLambda(1,:),x_point_it(i,:))
    hold on
end
ylabel('x\_point');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),x_point_it(i,:))
    plot(payment_it(1,:),x_point_it(i,:))
    hold on
end
ylabel('x\_point');

figure
for i=1:size(b_it,1)
    %     plot(b_it(i,:),(1-crps_it(i,:)).*b_it(i,:))
    plot(pay_scaledInLambda(1,:),(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
    hold on
end
ylabel('(1-crps).*pay\_scaledInLambda');


if includeOptimization == 1
    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:))
        plot(pay_scaledInLambda(1,:),obj_it(i,:))
        hold on
    end
    ylabel('objective');

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),totalPayment(i,:))
        plot(pay_scaledInLambda(1,:),totalPayment(i,:))
        hold on
    end
    ylabel('totalPayment');

    % figure
    % plot((1-crps_it)*lambda_dayAhead(1,1),totalPayment)

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        plot(pay_scaledInLambda(1,:),obj_it(i,:)+(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
        hold on
    end
    ylabel('obj+(1-crps).*pay\_scaledInLambda');

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
        hold on
    end
    ylabel('obj+(1-crps).*pay\_scaledInLambda');


    figure
    for i=1:size(b_it,1)
        %             plot(pay_concat(i,:),obj_it(i,:)+(1-crps_it(i,:)).*pay_concat(i,:))
        plot(payment_it(1,:),obj_it(i,:)+(1-crps_it(i,:)).*payment_it(1,:))
        hold on
    end
    ylabel('obj+(1-crps).*payment');


    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),totalPayment1(i,:)+(1-crps_it(i,:)).*b_it(i,:))
        plot(pay_scaledInLambda(1,:),totalPayment1(i,:)+(1-crps_it(i,:)).*pay_scaledInLambda(1,:))
        hold on
    end
    ylabel('totalPayment1+(1-crps).*pay\_scaledInLambda');

    figure
    for i=1:size(b_it,1)
        %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*lambda_DA)
        plot(pay_scaledInLambda(1,:),obj_it(i,:)+(1-crps_it(i,:)).*lambda_DA)
        hold on
    end
    ylabel('obj+(1-crps).*lambda\_DA');



    bb = totalPayment1(i,:)+(1-crps_it(i,:)).*b_it(i,:);
    min1 = find(bb== min(bb))

    min2 = find(totalPayment==min(totalPayment))

end
% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*b_it3(2:end))
%
% figure
% plot(b_it3(2:end),obj_it3(2:end)+1.*b_it3(2:end))
%
%
% figure
% plot(b_it3(2:end),obj_it3(2:end)+(1-crps_it3(2:end)).*lambda_DA)

% figure
% plot(b_it,obj_it+linspace(1,2,10))


%%

if includeOptimization == 1

    if size(resultsAll,1)>1

        figure
        %     for i1 = 1:size(resultsAll,1)
        for i1 = 1:1
            for i=1:size(pay_scaledInLambda,2)
                %         plot(b_it(i,:),obj_it(i,:)+(1-crps_it(i,:)).*b_it(i,:))
                objtemp1(1,i) = resultsAll(i1,i).objective;
                %             plot(pay_scaledInLambda(1,i),resultsAll(i1,i).objective+(1-crps_it(i1,i)).*pay_scaledInLambda(1,i))
                max1 = max(objtemp1);
            end
            plot(pay_scaledInLambda(1,:),objtemp1(1,:)+(1-crps_it(i1,:)).*pay_scaledInLambda(1,:))
            hold on
        end

        figure
        %         for i1 = 1:size(resultsAll,1)
        for i1 = 2:2
            plot(pay_scaledInLambda(1,:),totalPayment(i1,:))
            %         hold on
        end


    end

end





