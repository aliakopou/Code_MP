clear all 
close all

numOfScenarios = 100;

ValueSoThaConvexShapeIsAchieved = 1000;  % THIS IS TEMPORARY, UNTIL the extra payment becomes a function of the other costs

itNumber = 1;

% crps_temp = [0.9,0.7,0.5,0.3,0.01];
crps_temp = [0.1,0.7,0.5,0.3,0.1];

%% Loop 
% payVal=10;
% for payVal = [10,20000]
for payVal = [2,20,200,2000,20000]

    realizedPower = 150*10^6;

%     payVal = (1-crps_temp) *2000;

    [scens,probs] = getScenariosUsingInverseTransformSampling(payVal, numOfScenarios,realizedPower,0);

    % scens = realizedPower.*ones(1,numOfScenarios);


    % [objFunctionValueOut,optimSolverTime] = StochOptimization_scensFromCDFfunction(scens,probs,realizedPower);

    StochOptimization_revisited_singleTimestep

    plot1(itNumber) =  objectiveFunctionValue;

    

% end


%% Calculating extra payment as function of lambda DA            (1-crps_temp)*

addit = 0;

 for itt = 1:numOfScenarios
        addit = addit + (1.*(  lambda_dayAheadRT.scen1(1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt)/BasePower -  lambda_dayAheadRT.scen1(1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt)/BasePower ));
 end

 totalPayment(itNumber) = sum( lambda_dayAhead.scen1(1)*GeneratorDayAheadPower/BasePower  + lambda_dayAhead.scen1(1)*WindDayAheadPower/BasePower + (1/numOfScenarios)*addit );

 totalPayment(itNumber) = -totalPayment(itNumber) - (1-crps_temp(itNumber))*lambda_dayAhead.scen1(1);

% totalPayment(itNumber) = lambda_dayAhead.scen1(1) * GeneratorDayAheadPower + lambda_dayAhead.scen1(1) *WindDayAheadPower + lambda_dayAheadRT.scen1(1)/(1/scenarios) * GeneratorUpPower + ... 
%     lambda_dayAheadRT.scen1(1)/(1/scenarios) * GeneratorDownPower + lambda_dayAheadRT.scen1(1)/(1/scenarios) * (realizedPower - WindDayAheadPower);  

itNumber = itNumber+1;

payVal

end



figure
% plot([10,20000],plot1)
plot([2,20,200,2000,20000],plot1)


figure
% plot([10,20000],totalPayment)
plot([2,20,200,2000,20000],totalPayment)


