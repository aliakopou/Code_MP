% clear all
% close all

numOfScenarios = 100;

ValueSoThaConvexShapeIsAchieved = 1000;  % THIS IS TEMPORARY, UNTIL the extra payment becomes a function of the other costs
lambda_DA = 60;

itNumber2 = 1;

% for realizedPower = [100,110,120,130,140,150,160,170,180,190]*10^6
for realizedPower = [100*10^6,120*10^6,190*10^6]

    itNumber = 1;

    %% Loop
    % payVal=10;
    % for payVal = [10,20000]
%     for payVal = [2,20,200,2000]
    for b = linspace(2,lambda_DA,5)

        [scens,x_point]=sampleScenarios_normalizedDetailedCDFFunction(b, x_point_init, y_value, realization, lambda_DA, numberOfScens, UpperGenLimit, seedBool);

%         [scens,probs] = getScenariosUsingInverseTransformSampling(payVal, numOfScenarios,realizedPower,0);

        % scens = realizedPower.*ones(1,numOfScenarios);


        % [objFunctionValueOut,optimSolverTime] = StochOptimization_scensFromCDFfunction(scens,probs,realizedPower);

        StochOptimization_revisited_singleTimestep

%         plot1(itNumber) = payVal/ValueSoThaConvexShapeIsAchieved + objectiveFunctionValue;
        objFunValue_it(itNumber) = objectiveFunctionValue;


        % end


        %% Calculating extra payment as function of lambda DA            (1-crps_temp)*

        addit = 0;

        for itt = 1:numOfScenarios
            addit = addit + (1.*(  lambda_dayAheadRT.scen1(1)/(1/numOfScenarios)*GeneratorUpPower(:,:,itt) -  lambda_dayAheadRT.scen1(1)/(1/numOfScenarios)*GeneratorDownPower(:,:,itt) ));
        end

        totalPayment(itNumber) = sum( lambda_dayAhead.scen1(1)*GeneratorDayAheadPower  + lambda_dayAhead.scen1(1)*WindDayAheadPower + (1/numOfScenarios)*addit );

%         totalPayment(itNumber) = -totalPayment(itNumber)/BasePower + payVal/ValueSoThaConvexShapeIsAchieved;

        % totalPayment(itNumber) = lambda_dayAhead.scen1(1) * GeneratorDayAheadPower + lambda_dayAhead.scen1(1) *WindDayAheadPower + lambda_dayAheadRT.scen1(1)/(1/scenarios) * GeneratorUpPower + ...
        %     lambda_dayAheadRT.scen1(1)/(1/scenarios) * GeneratorDownPower + lambda_dayAheadRT.scen1(1)/(1/scenarios) * (realizedPower - WindDayAheadPower);

        itNumber = itNumber+1;

%         payVal

    end

    totalResults10Timesteps(itNumber2,:) = totalPayment;

    itNumber2 = itNumber2+1;

    realizedPower

end



figure
% plot([10,20000],plot1)
plot([2,20,200,2000,20000],plot1)


figure
% plot([10,20000],totalPayment)
plot([2,20,200,2000,20000],totalPayment)


