% function [objectiveFunctionValue,cal_time,lambda_dayAhead,c_rt] = StochOptimization_scensFromCDFfunction(scens,probs,realizedPower)
% for kk=1:1

% clear all
% close all
% clear variables        
% clc
yalmip('clear')

 clearvars -except realizedPower

%% Network characteristics

BaseVA = 100*10^6;
BasePower = 100*10^6;

x12 = 0.1; %pu
x13 = 0.3; %pu
x23 = 0.1; %pu


B = [(1/x12 + 1/x13) -1/x12 -1/x13
    -1/x12 (1/x12 + 1/x23) -1/x23
    -1/x13 -1/x23 (1/x13 + 1/x23)];


% rng(12) % seed for random generator

PL = (250/130)*realizedPower*10^0 * ones(1,24);  %scaling factor could be anything (here 250/130)
% PL = 150*10^6.*rand(1,24);

N = 3; % number of buses
NL = 3; % number of buses

timestep = 1; %number of discrete time points

%% Parameters/Limits

Pg1_max = (300/150)*realizedPower*10^0; %scaling factor could be anything

Pij_max = (500/130)*realizedPower*10^0; %scaling factor could be anything

it1 = 0;

% for iterVal = 32*10^6 :1*10^5:32*10^6
for iterVal = 1 :1:1
    
    % WindGenerationRealised = Pg2_max * ones(1,24).*(rand(1,24)*10);
    % WindGenerationRealised = Pg2_max * ones(1,24) * 0.9;
    % WindGenerationRealised = Pg2_max * ones(1,24) * 1;
    
    
    %     % Pg2_max = 200*10^6; %essentially your forecast
    %     % WindRealizationForecast = [5000*10^6 ; 30*10^6];
    %     randomVariations = rand(1,24);
    % %     WindRealizationForecast = [iterVal.*rand(1,24) ; 30*10^6.*randomVariations; 1*10^6.*rand(1,24)];
    % %     WindRealizationForecast = [iterVal.* rand(1,24) ; 30*10^6.*randomVariations];
    %     WindRealizationForecast = [31*10^6.*randomVariations ; 30*10^6.*randomVariations];
    %     [maxWind, indexMax] = max(WindRealizationForecast);
    %
    %     scenarios = size(WindRealizationForecast,1); %number of scenarios
    
    
    %     mu = 30;
    %     sigma = iterVal;
    %     sz = [1,100000];
    
    %     scens = SamplingScenarios(mu,sigma,sz);
    
    %     WindRealizationForecast = transpose(scens.BinEdges(2:end))*10^6*ones(1,24);
    %     WindRealizationForecast = transpose(scens.BinEdges(2:end))*10^6*ones(1,24)+ones(length(scens.BinEdges(2:end)),1)*randn(1,24);
%     rng(12) % seed for random generator
    WindRealizationForecast_nonSorted = realizedPower;

    WindRealizationForecast = sort(WindRealizationForecast_nonSorted);

%     WindRealizationForecast = realizedPower*ones(scenarios,1);  %added for testing purposes
    
    
    [maxWind, indexMax] = max(WindRealizationForecast);
    
    scenarios = 1; %number of scenarios
    
    probs = 1; %because we use SAA
    p = probs;
    
    
    %% Costs Definition
    cg = 60; % the generator 1 cost
    % c2 = 120;
%     c_rt = 100; % the regualtion cost (has to be more expensive than cg)
    c_u = 200; %up regulation cost (higher than DA)
    c_d = 20; %down regulation cost (higher than DA)
    cs = 650;  % the load shedding cost

    %% Regulation Constants 
    Pu_max = 0.9*Pg1_max;
    Pd_max = 0.9*Pg1_max;
    
    
    %% YALMIP Variable Declaration
    
    theta = sdpvar(N,timestep,scenarios,'full'); %Real Time
    theta_DA = sdpvar(N,timestep,scenarios,'full'); %Day Ahead
    % Pij = sdpvar(NL,timestep,scenarios,'full');
    Pg1 = sdpvar(1,timestep,1,'full');   %Day Ahead generator power
    Pw = sdpvar(1,timestep,1,'full');   %Day Ahead wind power
    % Vi = sdpvar(N,timestep,scenarios,'full');
    % Pg_da = sdpvar(1,timestep,'full');
    % Pwp_da = sdpvar(1,timestep,'full');
    Pu = sdpvar(1,timestep,scenarios,'full'); %Generator Up regulation power
    Pd = sdpvar(1,timestep,scenarios,'full'); %Generator Down regulation power
    Pspill = sdpvar(1,timestep,scenarios,'full');
    Pshed = sdpvar(1,timestep,scenarios,'full');
    
    
    %% Define Problem Constraints
    
    Constraints = [];
    
    %-- DA reference angle --%
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, theta_DA(1,tim,sc) == 0]; %reference angle DA
        end
    end

    %-- RT reference angle --%
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, theta(1,tim,sc) == 0]; %reference angle RT
        end
    end
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, Pg1(1,tim) <= Pg1_max/BasePower];
            Constraints = [Constraints, Pg1(1,tim) >= 0];
            Constraints = [Constraints, Pu(1,tim,sc) >= 0];
            Constraints = [Constraints, Pd(1,tim,sc) >= 0];
            Constraints = [Constraints, Pu(1,tim,sc) <= Pu_max/BasePower];
            Constraints = [Constraints, Pd(1,tim,sc) <= Pd_max/BasePower];
            Constraints = [Constraints, Pg1(1,tim) + Pu(1,tim,sc) - Pd(1,tim,sc) <= Pg1_max/BasePower];
            Constraints = [Constraints, Pg1(1,tim) + Pu(1,tim,sc) - Pd(1,tim,sc) >= 0];
            Constraints = [Constraints, Pw(1,tim) >= 0];
            Constraints = [Constraints, Pw(1,tim) <= maxWind(tim)/BasePower];
            %         Constraints = [Constraints, Pg1_rt(1,tim,sc) <= Pg1_max/BasePower];
            %         Constraints = [Constraints, Pg1_rt(1,tim,sc) >= -Pg1_max/BasePower];
        end
    end
    
    %-- Day Ahead Balance --%
    for sc = 1:scenarios
%         for tim = 1:timestep
            ConstraintName = sprintf('Constraint%d', sc);
            eval([ConstraintName ' =  B * theta_DA(:,:,sc) == [Pg1(1,:); Pw(1,:); -PL(1,1)/BasePower];']);
%             ConstraintName = B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower];
%             Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower]] ;
            Constraints = [Constraints, eval(ConstraintName)];
%             Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower]] ;
%         end
    end
    
    %-- Real Time Balance --%
%     for sc = 1:scenarios
% %         for tim = 1:timestep
%             Constraints = [Constraints, B * (theta(:,:,sc)-theta_zero(:,:,sc)) == [Pg1_rt(1,:,sc); WindRealizationForecast(sc,:)./BasePower - Pg2(1,:) - 1*Pspill(1,:,sc); Pshed(1,:,sc)]] ;
% %     end
%     end

     for sc = 1:scenarios
%         for tim = 1:timestep
            ConstraintNameRT = sprintf('ConstraintRT%d', sc);
            eval([ConstraintNameRT ' =  B * (theta(:,:,sc)-theta_DA(:,:,sc)) == [Pu(1,tim,sc) - Pd(1,tim,sc) ; WindRealizationForecast(sc,:)./BasePower - Pw(1,:) - 1*Pspill(1,:,sc); Pshed(1,:,sc)];']);
%             ConstraintName = B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower];
%             Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower]] ;
            Constraints = [Constraints, eval(ConstraintNameRT)];
%             Constraints = [Constraints, B * theta(:,tim,sc) == [Pg1(1,tim); Pg2(1,tim); -PL(tim)/BasePower]] ;
%         end
    end

    %-- Real Time Line Max Capapacity --%
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, (1/x12)*(theta(1,tim,sc) - theta(2,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta(2,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta(1,tim,sc) - theta(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x12)*(theta(1,tim,sc) - theta(2,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta(2,tim,sc) - theta(3,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta(1,tim,sc) - theta(3,tim,sc)) >= -Pij_max/BasePower];
        end
    end
    
    %-- Day Ahead Line Max Capapacity --%
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, (1/x12)*(theta_DA(1,tim,sc) - theta_DA(2,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta_DA(2,tim,sc) - theta_DA(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta_DA(1,tim,sc) - theta_DA(3,tim,sc)) <= Pij_max/BasePower];
            Constraints = [Constraints, (1/x12)*(theta_DA(1,tim,sc) - theta_DA(2,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x23)*(theta_DA(2,tim,sc) - theta_DA(3,tim,sc)) >= -Pij_max/BasePower];
            Constraints = [Constraints, (1/x13)*(theta_DA(1,tim,sc) - theta_DA(3,tim,sc)) >= -Pij_max/BasePower];
        end
    end
    
    for sc = 1:scenarios
        for tim = 1:timestep
            Constraints = [Constraints, 1*Pspill(1,tim,sc) <= WindRealizationForecast(sc,tim)/BasePower];
            Constraints = [Constraints, Pshed(1,tim,sc) <= PL(tim)/BasePower];
            Constraints = [Constraints, 1*Pspill(1,tim,sc) >= 0];  %non-negativity constraint
            Constraints = [Constraints, Pshed(1,tim,sc) >= 0];

        end
    end
    
    
%     for sc = 1:scenarios
%         for tim = 1:timestep
%             Constraints = [Constraints, Pg1_rt(1,tim,sc)+ Pshed(1,tim,sc) + (WindRealizationForecast(sc,tim)/BasePower - Pg2(1,tim) - 1*Pspill(1,tim,sc)) == 0]; %We want the deviations from the day ahead values to be zero
%         end
%     end
    
    %% Objective Function
    
    
    disp('Constraint definition has finished')
    
    %     p1 = 0.1;
    %     p2 = 0.9;
    %     p3 = 0.2;
    
    %     p = [0.1; 0.9];
    
    % cg = 20;
    % cs = 30;
    % cg_2 = 100;
    
    % Objective = sum(cg*Pg1(1,:,:) + 0*Pg2(1,:,:));
    
    %     Objective = (sum(cg*Pg1(1,:) + ...
    %         p1*(c_rt*abs(Pg1_rt(1,:,1))+cs*Pshed(1,:,1)) +...
    %         p2*(c_rt*abs(Pg1_rt(1,:,2))+cs*Pshed(1,:,2))));
    % %         p3*(c_rt*abs(Pg1_rt(1,:,3))+cs*Pshed(1,:,3))));
    
    
    add1 = 0;

%     for itt = 1:scenarios
%         add1 = add1 + (p(itt).*( c_rt*abs(Pg1_rt(1,:,itt))+cs*Pshed(1,:,itt) ));
%     end
% 
%     Objective = sum(cg*Pg1(1,:) + add1);


    %Sample Average Approximation
    for itt = 1:scenarios
        add1 = add1 + (1.*( c_u*Pu(1,:,itt) - c_d*Pd(1,:,itt) + cs*Pshed(1,:,itt) ));
    end

    Objective = sum( cg*Pg1(1,:) + (1/scenarios)*add1 )*1;
%     Objective = sum( cg*Pg1(1,:) + (1/scenarios)*add1 )*BasePower;
    
    
    
    % options = sdpsettings('verbose',1,'solver','ipopt');
    options = sdpsettings('verbose',1,'solver','gurobi');
    % options = sdpsettings('verbose',1,'solver','bmibnb');
    
    %Solving the problem
    sol = optimize(Constraints,Objective,options);
    
    
    cal_time = sol.solvertime;
    
    it1 = it1+1;
    
%     iterVal
    
    ObjIter(it1) = value(Objective);
    
    objectiveFunctionValue = value(Objective);
    
end

% Saving the lambda Day Ahead (=dual of balance constraints) values in a struct
for ind = 1:scenarios
    % Construct the constraint name
    ConstraintNm= ['Constraint',num2str(ind)];
    
    % Compute the dual variable for the constraint
    dual_value = dual(eval(ConstraintNm));
    
    % Save the dual variable to the struct lambda_dayAhead
%     lambda_dayAhead.(['scen',num2str(ind)]) = dual_value; 
    lambda_dayAhead(ind,:) = transpose(dual_value); 
end

% Saving the lambda Real Time values in a struct
for ind = 1:scenarios
    % Construct the constraint name
    ConstraintNmRT= ['ConstraintRT',num2str(ind)];
    
    % Compute the dual variable for the constraint
    dual_value = dual(eval(ConstraintNmRT));
    
    % Save the dual variable to the struct lambda_dayAhead
%     lambda_dayAheadRT.(['scen',num2str(ind)]) = dual_value;
    lambda_RT(ind,:) = transpose(dual_value);
end

% lambda_dayAhead.scen2 = dual(Constraint2);
% lambda_dayAhead.scen3 = dual(Constraint3);
% lambda_dayAhead.scen4 = dual(Constraint4);
% lambda_dayAhead.scen5 = dual(Constraint5);
% lambda_dayAhead.scen6 = dual(Constraint6);
% lambda_dayAhead.scen7 = dual(Constraint7);
% lambda_dayAhead.scen8 = dual(Constraint8);
% lambda_dayAhead.scen9 = dual(Constraint9);
% lambda_dayAhead.scen10 = dual(Constraint10);

% lambda_dayAheadRT.scen1 = dual(Constraint1RT);
% lambda_dayAheadRT.scen2 = dual(Constraint2RT);
% lambda_dayAheadRT.scen3 = dual(Constraint3RT);
% lambda_dayAheadRT.scen4 = dual(Constraint4RT);
% lambda_dayAheadRT.scen5 = dual(Constraint5RT);
% lambda_dayAheadRT.scen6 = dual(Constraint6RT);
% lambda_dayAheadRT.scen7 = dual(Constraint7RT);
% lambda_dayAheadRT.scen8 = dual(Constraint8RT);
% lambda_dayAheadRT.scen9 = dual(Constraint9RT);
% lambda_dayAheadRT.scen10 = dual(Constraint10RT);

%% Results

GeneratorDayAheadPower_pu = value(Pg1); 
% GeneratorBalancePower = value(Pg1_rt*BasePower);
GeneratorUpPower_pu = value(Pu);
GeneratorDownPower_pu = value(Pd);
WindDayAheadPower_pu = value(Pw); 
SpilledValue_pu = value(Pspill);
ShededValue_pu = value(Pshed);


GeneratorDayAheadPower = value(Pg1*BasePower); 
% GeneratorBalancePower = value(Pg1_rt*BasePower);
GeneratorUpPower = value(Pu*BasePower);
GeneratorDownPower = value(Pd*BasePower);
WindDayAheadPower = value(Pw*BasePower); 
SpilledValue = value(Pspill*BasePower);
ShededValue = value(Pshed*BasePower);

% scenNo = 1;
% RevenueWF_scen1 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen1(1)/(1/scenarios));
% 
% scenNo = 3;
% RevenueWF_scen3 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen3(1)/(1/scenarios));
% 
% scenNo = 4;
% RevenueWF_scen4 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen4(1)/(1/scenarios));
% 
% scenNo = 5;
% RevenueWF_scen5 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));
% 
% scenNo = 8;
% RevenueWF_scen8 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen8(1)/(1/scenarios));
% 
% scenNo = 10;
% RevenueWF_scen10 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen10(1)/(1/scenarios));
% 
% scenNo = 11;
% RevenueWF_scen11 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen11(1)/(1/scenarios));
% 
% scenNo = 15;
% RevenueWF_scen15 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));
% 
% scenNo = 16;
% RevenueWF_scen16 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));
% 
% scenNo = 17;
% RevenueWF_scen17 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));
% 
% scenNo = 18;
% RevenueWF_scen18 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));
% 
% scenNo = 20;
% RevenueWF_scen20 = WindDayAheadPower*lambda_dayAhead.scen1(1) + (scens(scenNo)-WindDayAheadPower)*(lambda_dayAheadRT.scen5(1)/(1/scenarios));

% size(Objective)
% value(Objective)
% value(Pg1*BasePower)
% value(Pg2*BasePower)
% value(Pg1_rt*BasePower)
% value(Pshed*BasePower)
% value(Pspill*BasePower)
%
%
% figure
% plot(1:24, value(Pg1*BasePower))
% hold on
% plot(1:24, value(Pg2*BasePower))
% hold on
% plot(1:24, value(Pg1_rt*BasePower))
% hold on
% plot(1:24, value(Pshed*BasePower))
% hold on
% plot(1:24, value(Pspill*BasePower))
% xlim([1 23])
%
% figure
% plot(1:24, WindGenerationRealised)
% hold on
% plot(1:24, Pg2_max*ones(1,24))
% xlim([1 24])


%%
% figure
% plot(1:length(ObjIter),ObjIter)
% xlim([2 length(ObjIter)])

% end