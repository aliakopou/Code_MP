clear all
close all
clear variables
yalmip('clear')
%%

initPayment = 1+1/625;
payment(1) = initPayment;
payment(2) = 1 + 0.15;

mu = 130;
sz = [1,100000];

% ref = 2.766550131946314e+03; %obtained by paying a very high payment value (here 100000)

loopIndex1 = 1;
l=1;
stopCriterion = 0;

numOfScenarios = 20;

% realizedPower = 130; %value given in MW

%%

% numberOfBinsApproximation = 10;

% for numberOfBinsApproximation = [10,100,1000,10000,100000,1000000]
% for numberOfBinsApproximation = 10

% for realizedPower = [10, 130, 1000]
for realizedPower = 130 %value given in MW
    
    l=1;
    stopCriterion = 0;
    
    initPayment = 1+1/625;
    payment(1) = initPayment;
    payment(2) = 1 + 0.15;
    
    bValue1(1) = payment(1)^2;
    bValue1(2) = payment(2)^2;
    
    bValue2(1) = sqrt(payment(1));
    bValue2(2) = sqrt(payment(2));
    
    [ref,calc1] = StochOptimization_getRefObjFunction_2WF(realizedPower); %calculate objective function value when no uncertainty
    
    while stopCriterion==0
        
        %%--Define function of payment for each Wind Farm--%%
        
%         bValue1(l) = payment(l)^2;
        bValue1(1) = payment(1)^2;
        bValue1(2) = payment(2)^2;
        
        %         bValue2(l) = sqrt(payVal);
%         bValue2(l) = sqrt(payment(l));
        bValue2(1) = sqrt(payment(1));
        bValue2(2) = sqrt(payment(2));
        
        
        %     for itVal=1:1000:10000
        %     for payVal=[10,100,1000]
        %         for itVal2=1:20:110
        
        % sigma = iterVal;
        %     if l>=3
        %         sigma = getSigmaOfDistribution(abs(payment(l-1)));
        %     else
        %         sigma = getSigmaOfDistribution(payment(l));
        %     end
        
        %     scens = SamplingScenarios(mu,sigma,sz,numberOfBinsApproximation);
        
        %     logic that computes new cdf value given the payment and then gets the pdf and logic that computes wind power scenarios that then gives them into the stoch optimiz problem
        
        %     b = itVal;
        
        alpha = 1; %learning rate
        
        if l>=3
            der1(l) = ( ((objFunctionValue(l-2)- (bValue1(l-2)+bValue2(l-2)))-ref) - ((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref) ) / (  (bValue1(l-2)) - ( (bValue1(l-1))) );
            
            bValue1(l) =  (bValue1(l-1)) - alpha*( ((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref) /der1(l));
        end
        
        if l>=3
            der2(l) = ( ((objFunctionValue(l-2)- (bValue1(l-2)+bValue2(l-2)))-ref) - ((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref) ) / (  (bValue2(l-2)) - ( (bValue2(l-1))) );
            
            bValue2(l) =  (bValue2(l-1)) - alpha*( ((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref) /der2(l));
        end
        
        %%--Define function of payment for each Wind Farm--%%
        
%         bValue1(l) = payment(l)^2;
        
        
        %         bValue2(l) = sqrt(payVal);
%         bValue2(l) = sqrt(payment(l));
        
        
        
        
        %     if l>=3
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l-1), numOfScenarios);
        %     else
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios);
        % %         [scens,probs] = getScenariosFromCDFfunction(300, numOfScenarios);
        %     end
        
        %         [scens,probs] = getScenariosFromCDFfunction(payment(l), numOfScenarios,realizedPower);
        
        
        
        [scens,probs] = getScenariosFromCDFfunction(bValue1(l), numOfScenarios,realizedPower);
        
        [scens2,probs2] = getScenariosFromCDFfunction(bValue2(l), numOfScenarios,realizedPower);
        
        
        [objFunctionValueOut,optimSolverTime] = StochOptimization_scensFromCDFfunction_2WF(scens,scens2,probs,probs2,realizedPower);
        
        %     if objFunctionValueOut==0  %returned when the optim problem is infeasible
        %         objFunctionValueOut = ref*2; %a very big value
        %     end
        
        
        objFunctionValue(l) = objFunctionValueOut;
        %         paymentVal(l) = payVal;
        
        
        
        %%--Gradient Descent Implementation--%%
        
        %     if l==1
        %         payment(l) = initPayment;
        %     elseif l>=2
        %         payment(l) = payment(l-1) - alpha*(((objFunctionValue(l)+payment(l-1))-(objFunctionValue(l-1)+payment(l-1)))/(payment(l-1)));
        %     end
        
        
        %     if payment(l) < 0
        %         payment(l) = payment(l-1);
        %     end
        if l>3
            if ( ((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref)-((objFunctionValue(l)- (bValue1(l)+bValue2(l)))-ref) )<=0.5 && ...
                    (((objFunctionValue(l-1)- (bValue1(l-1)+bValue2(l-1)))-ref)-((objFunctionValue(l)- (bValue1(l)++bValue2(l)))-ref))>=0
                stopCriterion =1;
            end
        end
        
        
        payment
        bValue1
        bValue2
        objFunctionValue
        l
        
        
        solverTimeVaryingNumberOfScens(l) = optimSolverTime;
        
        
        
        l=l+1;
        
        %     end
    end
    
    AllResults(loopIndex1).realizedPower = realizedPower;
    AllResults(loopIndex1).objFunctionValue = objFunctionValue;
    AllResults(loopIndex1).payment = payment;
    AllResults(loopIndex1).solverTimeVaryingNumberOfScens = solverTimeVaryingNumberOfScens;
    
    loopIndex1 = loopIndex1 +1;
    %     end
end

%%
figure
plot( bValue1,objFunctionValue-(ref.*ones(1,length(objFunctionValue)))+bValue1 )
xlabel('sum of WF payments')