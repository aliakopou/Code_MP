% CRPS Formula Testing 
close all
clear all 


%% #1: Sample from CDF to create the realization 

MaxWind = 200*10^6;

b = 2;
x_point = 0.5;
y_value = 0.5;

numberSamples = 1;
powerVal = x_point * MaxWind;


visualizeCustomCDF(b,x_point,y_value)

for nn = 1:24
    [res1,res2] = getScenariosUsingInverseTransformSampling(b,numberSamples,powerVal,0);
    SamplesAll(nn) = res1;
end

figure
scatter(SamplesAll,ones(1,length(SamplesAll)))


%% #2: Compute Integral


WindRealizationValue = SamplesAll(1);

g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);


fun1 = @(x) (1./(1+(x.^g./(1-x.^g)).^(-b)) - 0).^2;

fun2 = @(x) (1./(1+(x.^g./(1-x.^g)).^(-b)) - 1).^2;

q1 = integral(fun1,0,WindRealizationValue/MaxWind);

q2 = integral(fun2,WindRealizationValue/MaxWind,1);

CRPS_value = q1 + q2;
