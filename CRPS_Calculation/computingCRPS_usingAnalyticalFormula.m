function crps = computingCRPS_usingAnalyticalFormula(optionNumber,lambda_DA,b,y_value,realization,x_point_init,plotCDF,use_payment_scaled)

%% Option 1: no modifications to originally defined custom cdf analytical function
if optionNumber == 1
    % Parameters
    b = 2; % Set the value for b
    y_value = 0.5;
    x_point = 0.2;

    realization = 0.9;
    % realization_value = 0.5;

    gridDiscretization = 10000;

    % Custom CDF Function y
    g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
    y = @(x) 1 / (1 + (x^g / (1 - x^g))^(-b));

    % Calculate CDFs
    x_values = linspace(0, 1, gridDiscretization); % Generate 1000 points between 0 and 1
    forecast_cdf = arrayfun(y, x_values); % Compute forecast CDF for each x

elseif optionNumber == 2
    %% Option 2: modification: Making x_point a function of b (linear interpolation)
    % for b = [100:100:2000]
    % Parameters
    b = 1000; % Set the value for b
    y_value = 0.5;
    x_point_init = 0.7; % Initial x_point value

    realization = 0.5;
    old_b_max = 2000; % Maximum value of b

    % Calculate x_point as a function of b
    x_point_ratio = b / old_b_max;
    x_point = x_point_init + (realization - x_point_init) * x_point_ratio;

    % Custom CDF Function y
    g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
    y = @(x) 1 / (1 + (x^g / (1 - x^g))^(-b));

    % Calculate CDFs
    x_values = linspace(0, 1, gridDiscretization); % Generate 1000 points between 0 and 1
    forecast_cdf = arrayfun(y, x_values); % Compute forecast CDF for each x

elseif optionNumber==3
    %% Option 3: modification: Making x_point a function of b (curvature power)
    % itNumm = 1;
    % for b = [100:100:2000]

    % Parameters
    % b = 20; % Set the value for b
    y_value = 0.5;
    x_point_init = 0.7; % Initial x_point value

    realization = 0.5;
    old_b_max = 2000; % Maximum value of b
    curvature_power = 1/4; % Power-law exponent to control curvature

    % Calculate x_point as a function of b
    x_point_ratio = b / old_b_max;
    curvature_factor = x_point_ratio^curvature_power;
    x_point = x_point_init + (realization - x_point_init) * curvature_factor;

    % Custom CDF Function y
    g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
    y = @(x) 1 / (1 + (x^g / (1 - x^g))^(-b));

    % Calculate CDFs
    x_values = linspace(0, 1, gridDiscretization); % Generate 1000 points between 0 and 1
    forecast_cdf = arrayfun(y, x_values); % Compute forecast CDF for each x

elseif optionNumber==4
    %% Option 4: modification: Making x_point a function of b and scaling max b to be equal to lambda_DA
    itNumm = 1;
    % lambda_DA = 60;
    % for b = 1:1:lambda_DA

    % Parameters
    b_orig = b; % Set the value for b (range: 1 to 60(=lambda_DA))

    old_b_max = 2000;

    % Scale b value from [1, 60(=lambda_DA)] to [1, 2000]
    b_scaled = 1 + ((b_orig - 1)/(lambda_DA-1))*(old_b_max-1);

    % y_value = 0.5;
    % realization = 0.5;
    % x_point_init = 0.7; % Set the value for x_point_init
    curvature_power = 3; % Set the value for curvature_power

    % Calculate x_point as a function of b_scaled
    curvature_factor = (b_scaled/old_b_max)^curvature_power;
    % curvature_factor = (b_orig/60)^curvature_power;
    x_point = x_point_init + (realization - x_point_init) * curvature_factor;

    % Set the value for g
    g = -log((1/y_value - 1)^(1/b_scaled)+1)/log(x_point);

    % Function y(x)
    y = @(x) 1 / (1 + (x^g / (1 - x^g))^(-b_scaled));

    % Calculate CDFs
    x_values = linspace(0, 1, 1000); % Generate 1000 points between 0 and 1
    forecast_cdf = arrayfun(y, x_values); % Compute forecast CDF for each x

else
    %% Option 5: modification: Using x_tilde and scaling max b to be equal to lambda_DA
    itNumm = 1;
    % lambda_DA = 60;
    % for b = 1:1:lambda_DA

    if use_payment_scaled==1
        % Parameters
        b_orig = b; % Set the value for b (range: 1 to 60(=lambda_DA))

        old_b_max = 2000;
        new_b_max = lambda_DA;

        % Scale b value from [1, 60(=lambda_DA)] to [1, 2000]
        b_scaled = 1 + ((b_orig - 1)/(new_b_max-1))*(old_b_max-1);
    else
        b_scaled = b;
    end
    % y_value = 0.5;
    % realization = 0.5;
    % x_point_init = 0.7; % Set the value for x_point_init
    % curvature_power = 3; % Set the value for curvature_power

    % Calculate x_point as a function of b_scaled
    % curvature_factor = (b_scaled/b_max)^curvature_power;
    % curvature_factor = (b_orig/60)^curvature_power;
    x_point = x_point_init;

    % y_value = x_point;

    % Set the value for g
    g = -log((1/y_value - 1)^(1/b_scaled)+1)/log(x_point);

    % Function y(x)
    y = @(x) 1 / (1 + (x^g / (1 - x^g))^(-b_scaled));

    % Calculate CDFs
    x_values = linspace(0, 1, 1000); % Generate 1000 points between 0 and 1
    forecast_cdf = arrayfun(y, x_values); % Compute forecast CDF for each x

    %% Compute CRPS value

    % Initialize CRPS
    crps = 0;

    % Compute realization CDF
    % realization = 0.5;
    realization_cdf = double(x_values >= realization);

    % Calculate squared difference
    squared_diff = (forecast_cdf - realization_cdf).^2;

    % Compute CRPS (integral of squared difference)
    crps = trapz(x_values, squared_diff);

    crps_it(itNumm) = crps;
    x_point_it(itNumm) = x_point;

end

%% Visualization
if plotCDF ==1
    if itNumm==1
        figure
    else
        hold on
    end
    % for b = 2
    % for b = [1,2,5,2000]
    % for b = [1/10,1/4]
    % g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);
    %
    % y=@(x) 1/(1+(x^g/(1-x^g))^-b);

    fplot(y,[0 1])
    % hold on
    % end

    xlim([0 1])
    ylim([0 1])

    itNumm = itNumm+1;
end
% end

%Visualize change of distribution mean
% figure
% plot(1:length(x_point_it),x_point_it)
