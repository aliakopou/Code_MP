function crps = compute_crps(forecast, observed)
% This function computes the Continuous Ranked Probability Score (CRPS)
% for a given forecast and observed data.

% forecast: a vector containing the forecast values
% observed: a scalar containing the observed value

n = length(forecast);
rank_forecast = tiedrank(forecast); % tie-breaking method

crps = 0;

for i = 1:n
    if rank_forecast(i) == 1
        crps = crps + (forecast(i) - observed)^2;
    else
        crps = crps + (forecast(i) - observed)^2 - (forecast(rank_forecast(i-1)) - observed)^2;
    end
end

crps = crps / n;
end
