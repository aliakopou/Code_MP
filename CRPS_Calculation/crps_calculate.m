function CRPS_value = crps_calculate(payment,realizationValue,x_point,y_value,MaxWind)

b=payment;
WindRealizationValue = realizationValue;

g = -log((1/y_value - 1)^(1/b)+1)/log(x_point);


fun1 = @(x) (1./(1+(x.^g./(1-x.^g)).^(-b)) - 0).^2;

fun2 = @(x) (1./(1+(x.^g./(1-x.^g)).^(-b)) - 1).^2;

q1 = integral(fun1,0,WindRealizationValue/MaxWind);

q2 = integral(fun2,WindRealizationValue/MaxWind,1);

CRPS_value = q1 + q2;