function crps = compute_crps2(cdf_observed, cdf_forecast, x_values)
% This function computes the Continuous Ranked Probability Score (CRPS)
% of two cumulative distribution functions (CDFs).

% cdf_observed: vector containing the observed CDF values
% cdf_forecast: vector containing the forecasted CDF values
% x_values: vector containing the corresponding x-values of the CDFs

n = length(x_values);
crps = 0;

for i = 1:n
    for j = 1:n
        crps = crps + (cdf_forecast(i) - cdf_observed(j))^2;
    end
end

crps = crps / (n^2) + (1 / (2 * n));
end