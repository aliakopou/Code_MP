% Generate some example forecast and observed data
% forecast = transpose(randn(100, 1));
% observed = 0.5;
% forecast = rand(1, 100);
forecast = scens/(2*realizedPower);
% forecast = aa23;
observed = realizedPower/(2*realizedPower);
forecast = 0.5*rand(1, 100);
observed = 0.7;
[meanCRPS2] = crps_readyFunc(forecast,observed)
% Compute the CRPS
% crps = compute_crps_final(forecast, observed);
%
% disp(['CRPS = ' num2str(crps)]);
% [meanCRPS] = crps_readyFunc(forecast,observed)
% %Working Example
% fcst = rand(1,1000);
% obs = rand(1,1);
% [meanCRPS] = crps_readyFunc(fcst,obs);