
paramSweepFig = 0;

if paramSweepFig==1
    clearvars -except rand_num randd
else
    clear all
end

paramSweepFig = 0;

seedVal = 10;

%% Load Data

load('log5_it200_06')

%% Input Data preprocess
totMeanFunc = mean(obj_it(:,:))+(1-mean(crps_it(:,:))).*pay_scaledInLambda(1,:);
% figure
% plot(payment_it(1,:),totMeanFunc)

incr1 = 1;
% for tempV = 10:10:size(obj_it,1)
for tempV = size(obj_it,1):10:size(obj_it,1)
    clear minn minIndex tempp
    % for ii = 1:size(obj_it,1)
    for ii = 1:tempV
        tempp = obj_it(ii,:)+(1-crps_it(ii,:)).*pay_scaledInLambda(1,:);
        temppAll(ii,:) = tempp;
        [minn(ii,:),minIndex(ii,:)] = min(tempp);
    end

    minn(:,:)
    optPayment = payment_it(minIndex);

    meanP(incr1) = mean(optPayment); % optimal mean payment value as number of MC increases
    % mean(minIndex(:,1))

    incr1 = incr1 +1;
end
%% Initialize

initialPayment = 1160;
optimalP = 11;
objT = 99.81;

total_Iterations = 1e+3;
epsilon = 0.1;

payment_arms = pay_concat;
% payment_arms = [4,8,10,11,150,1160];
% payment_arms = [1 11 20 30 40 150 550 1160 1520 1920];
% payment_arms = [1 11 1160 1520 1920];
num_payment_arms = length(payment_arms);

% historical_crps = [0, 0, 0.017, 0, 0, 0]; % historical average
% historical_crps = [0, 0, 0, 0, 0, 3.79e-04]; % historical average
historical_crps = zeros(1,num_payment_arms);
nonZeroIndexOrig = find(pay_concat == initialPayment);
nonZeroIndex = find(payment_arms == initialPayment);
historical_crps(nonZeroIndex) = 3.79e-04;

% historical_obj_func = [96.73+(96.73*0.5), 96.73+(96.73*0.5), 96.73, 96.73+(96.73*0.5), 96.73+(96.73*0.5), 96.73+(96.73*0.5)]; % historical average
% historical_obj_func = [99.81+(99.81*0.5), 99.81+(99.81*0.5), 99.81+(99.81*0.5), 99.81+(99.81*0.5), 99.81+(99.81*0.5), 99.81]; % historical average
historical_obj_func = zeros(1,num_payment_arms);
for il = 1: num_payment_arms
    if il == nonZeroIndex
        historical_obj_func(il) = objT;
    else
        historical_obj_func(il) = objT+(0.5*objT);
    end
end

% historical_counts = [0,0,1, 0, 0, 0];
% historical_counts = [0,0,0, 0, 0, 1];
historical_counts = zeros(1,num_payment_arms);
historical_counts(nonZeroIndex) = 1;


total_crps = historical_crps;
total_obj_func = historical_obj_func;
total_counts = historical_counts;

exploitationYes = zeros(total_Iterations,1);
%% Main Algorithm

numOfExploitations = 0;
numOfExplorations = 0;

if paramSweepFig==0
    rng(seedVal)
    rand_num = rand(1,total_Iterations); %generate a random number array between 0 and 1

    rng(seedVal)
    randd = randi(200,[1,total_Iterations]);
end

lambda_DA = 60;

pay_max = 2000;
pay_scaledInLambda = ((payment_arms - 1) / (pay_max - 1)) * (lambda_DA - 1) + 1; % Value in range: [1 to lambda_DA]


if paramSweepFig==1
    epsilon = 0.2;  %here again for parameter sweep
end

for iterat = 1:total_Iterations %repeat for every timestep


    %%-- Choosing action --%%

    ind_it = randd(iterat);

    estimated_reward(iterat,:) = total_obj_func(:)./ total_counts(:) + pay_scaledInLambda(:) .* (1-total_crps(:)./ total_counts(:)) ;


    if rand_num(iterat) > epsilon || iterat == 1 % Exploitation: choose payment value with the highest average reward
        [min_reward, index_min_reward] = min(estimated_reward(iterat,:));
        payment_action(iterat) = payment_arms(index_min_reward);

        % disp('Exploitation was chosen in this iteration')
        numOfExploitations = numOfExploitations+1;

        exploitationYes(iterat) = 1;
    
    p=0.1;
    else % Exploration: payment value is random
        if rand()<p
            [~, index_closest] = min(abs(payment_arms - runningAvgOptPayment(iterat-1)));
            payment_action(iterat) = payment_arms(index_closest);
        else
            random_index = randi(num_payment_arms); %generate a random index

            payment_action(iterat) = payment_arms(random_index); %choose the random element
        end

        % disp('Exploration was chosen in this iteration')
        numOfExplorations = numOfExplorations+1;
    end


    %%-- Using payment to create scens, calculate crps and calling SOPF --%%
    % useExistingxpoint=0;
    % includeOptimization = 1;
    % relationship_payment_b = 3;
    % seedBool = 1;
    % useRandomSeed = 1;
    % use_payment_scaled = 0;
    % includeRealizationInScenarios = 0;
    % plotCDF=0;
    %
    % UpperGenLimit = 2*150*10^6;
    % y_value = 0.5;
    % lambda_DA = 60;
    % realization = 0.6;
    % realizedPower = realization*UpperGenLimit;
    % numOfScenarios = 100;
    %
    % payment = payment_action;
    %
    % pipelineAfterChoosingPayment




    %%-- Using existing data --%%
    indPaymentData = find(pay_concat == payment_action(iterat),1);
    crps = crps_it(ind_it,indPaymentData);
    objectiveFunctionValue = obj_it(ind_it,indPaymentData);


    %%-- Updating the running total values --%%
    ind1 = find(payment_arms==payment_action(iterat));
    total_crps(ind1) = total_crps(ind1) + crps;
    total_obj_func(ind1) = total_obj_func(ind1) + objectiveFunctionValue;
    total_counts(ind1) = total_counts(ind1) + 1;

    tempMat(iterat) = sum(payment_action==optimalP)/iterat;


    current_reward(iterat) = objectiveFunctionValue + pay_scaledInLambda(find(payment_arms == payment_action(iterat),1))*(1-crps);


    %%-- Calculate the regret --%%
    optimal_arm(iterat) = optPayment(ind_it);
    optimal_reward(iterat) = minn(ind_it);
    regret =  current_reward(iterat) - optimal_reward(iterat) ;
    regrets(iterat) = regret;

    runningAvgRewOpt(iterat) = mean(optimal_reward);
    runningAvgRew(iterat) = mean(current_reward);

    runningAvgOptPayment(iterat) = mean(optimal_arm); %non-computationally efficient method
    % runningAvgOptPayment = (runningAvgOptPayment * (iterat - 1) + optimalP) / iterat; %computationally efficient method


    % regret_avg = runningAvgRew - runningAvgRewOpt;
    % % if iterat>=100
    % % avgrewlastfew(iterat) = mean(current_reward(iterat-10:iterat));
    % regret_threshold =100;
    % % Adjust epsilon based on regret
    % % if regret > 1
    % % if current_reward(iterat) > avgrew + avgrew*0.01
    % % if avgrew(iterat) > avgrewopt(iterat) + avgrewopt(iterat)*0.1
    % % if regret >0.01 * avgrewlastfew(iterat)
    % if regret >0.01 *optimal_reward(iterat)
    %     epsilon = min(epsilon * 1.1, 1); % Increase epsilon
    % else
    %     epsilon = max(epsilon * 0.9, 0); % Decrease epsilon
    % end
    % % end
    % epsilon_overTime(iterat) = epsilon;

end


%% Optimal Values
FuncTemp1 = estimated_reward(end,:);
indTemp1 = find(estimated_reward(end,:)==min(estimated_reward(end,:)));

optArm = payment_arms(indTemp1)

analyzeTempMat = cat(2,estimated_reward,payment_action.',optimal_arm.',exploitationYes);

%Plot expoitation values
% Find indices where Ar2 is equal to 1
selected_indices = analyzeTempMat(:,end) == 1;

% Extract the corresponding elements from Ar1
selected_values = analyzeTempMat(selected_indices,end-1);

% figure
% scatter(1:length(selected_values),selected_values)

if paramSweepFig==0
    figure
else
    hold on
end
plot(1:total_Iterations,tempMat)
xlabel('Time Step')
ylabel('Mean Rate of Choosing Best Arm')


%%

%%-- Check convergence of optimal payment value over iterations
for N = 10:10:total_Iterations
    mean_values(N/10) = mean(optimal_arm(1:N));
end


figure
plot(10:10:total_Iterations, mean_values, '-o');
hold on
plot(10:10:total_Iterations, round(mean_values), '-.');
xlabel('Number of Elements (N)');
ylabel('Mean');


