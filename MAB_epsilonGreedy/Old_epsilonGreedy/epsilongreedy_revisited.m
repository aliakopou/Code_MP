clear all

%% Initialize 


payment_arms = [4,8,10,100,1000];
num_payment_arms = length(payment_arms); 
historical_crps = [0, 0, 0.017, 0, 0]; % historical average
historical_obj_func = [96.73+(96.73*0.5), 96.73+(96.73*0.5), 96.73, 96.73+(96.73*0.5), 96.73+(96.73*0.5)]; % historical average
historical_counts = [0,0,1, 0, 0]; 

total_Iterations = 100; 
epsilon = 0.3; 

total_crps = historical_crps;
total_obj_func = historical_obj_func;
total_counts = historical_counts; 

%% Main Algorithm

numOfExploitations = 0;
numOfExplorations = 0;

rand_num = rand(1,total_Iterations); %generate a random number array between 0 and 1 

for iterat = 1:total_Iterations %repeat for every timestep

    %%-- Choosing action --%%

    
    estimated_reward(iterat,:) = (total_obj_func(:) + payment_arms(:) .* (1-total_crps(:)) )./ total_counts(:);
 

    if rand_num(iterat) >= epsilon % Exploitation: choose payment value with the highest average reward 
        [min_reward, index_min_reward] = min(estimated_reward(iterat,:));
        payment_action = payment_arms(index_min_reward)

        disp('Exploitation was chosen in this iteration')
        numOfExploitations = numOfExploitations+1;

    else % Exploration: payment value is random
        
        random_index = randi(num_payment_arms); %generate a random index
         
        payment_action = payment_arms(random_index) %choose the random element

        disp('Exploration was chosen in this iteration')
        numOfExplorations = numOfExplorations+1;
    end        


    %%-- Using payment to create scens, calculate crps and calling SOPF --%%
    useExistingxpoint=0;
    includeOptimization = 1;
    relationship_payment_b = 3;
    seedBool = 1;
    useRandomSeed = 1;
    use_payment_scaled = 0;
    includeRealizationInScenarios = 0; 
    plotCDF=0;
    
    UpperGenLimit = 2*150*10^6;
    y_value = 0.5;    
    lambda_DA = 60;
    realization = 0.6;
    realizedPower = realization*UpperGenLimit;
    numOfScenarios = 100;

    payment = payment_action; 

    pipelineAfterChoosingPayment
    



    %%-- Updating the running total values --%%
    ind1 = find(payment_arms==payment_action); 
    total_crps(ind1) = total_crps(ind1) + crps; 
    total_obj_func(ind1) = total_obj_func(ind1) + objectiveFunctionValue;
    total_counts(ind1) = total_counts(ind1) + 1;



end