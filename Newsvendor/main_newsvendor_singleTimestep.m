clear all
close all
clear variables
yalmip('clear')

%% Parameters

numberOfScens = 10000;

% realizedPower = 130; % value given in MW
realizedPower = 50/10^6; % value given in MW

%%

tic

for payVal=2:1:2
    
%     [scens,probs] = getScenariosFromSimplifiedCDFfunction(payVal, numberOfScens, realizedPower);
%     [scens,probs] = getScenariosUsingInverseTransformSampling(payVal, numberOfScens, realizedPower*10^6);
    [scens,probs] = getScenariosUsingInverseTransformSampling2(payVal, numberOfScens, realizedPower*10^6);
    
    
    [objectiveFunctionValue, cal_time,x] = NewsvendorAsSLP_ProductionSide_singleTimestep(scens,probs);
    
    
end

value(x)

toc