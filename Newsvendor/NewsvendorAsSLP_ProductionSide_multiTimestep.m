function [objectiveFunctionValue,cal_time,x] = NewsvendorAsSLP_ProductionSide(scens,probs)

% clear all
% close all
% clear variables
% clc
yalmip('clear')

%% Parameters

timestep = 24; %number of discrete time points

% scenarios = length(scens);

rng(12) % seed for random generator
WindRealizationForecast = transpose(scens)*ones(1,24).*rand(1,24);


scenarios = size(WindRealizationForecast,1); %number of scenarios

p = probs;


c = 10;
delta = 5;
eta = 2;

%% YALMIP Variable Declaration

% x, y1k, y2k, y3k

x = sdpvar(1,timestep,'full'); % Day ahead quantity x
% Pij = sdpvar(NL,timestep,scenarios,'full');
% Pg1 = sdpvar(1,timestep,1,'full');   %Day Ahead generator power
% Pg2 = sdpvar(1,timestep,1,'full');   %Day Ahead wind power
% Vi = sdpvar(N,timestep,scenarios,'full');
% Pg_da = sdpvar(1,timestep,'full');
% Pwp_da = sdpvar(1,timestep,'full');
y1 = sdpvar(1,timestep,scenarios,'full'); % the quantity of energy produced
y2 = sdpvar(1,timestep,scenarios,'full'); % the quantity of energy missing
y3 = sdpvar(1,timestep,scenarios,'full'); % the surplus of energy


%% Define Problem Constraints

Constraints = [];

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, y1(1,tim,sc) + y2(1,tim,sc) == x(1,tim)]; 
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, y1(1,tim,sc) + y3(1,tim,sc) == WindRealizationForecast(sc,tim)];
    end
end

for sc = 1:scenarios
    for tim = 1:timestep
        Constraints = [Constraints, x(1,tim) >= 0];
        Constraints = [Constraints, y1(1,tim,sc) >= 0]; 
        Constraints = [Constraints, y2(1,tim,sc) >= 0];
        Constraints = [Constraints, y3(1,tim,sc) >= 0];
    end
end


%% Objective Function


disp('here')

%     p1 = 0.1;
%     p2 = 0.9;
%     p3 = 0.2;

%     p = [0.1; 0.9];

% cg = 20;
% cs = 30;
% cg_2 = 100;

% Objective = sum(cg*Pg1(1,:,:) + 0*Pg2(1,:,:));

%     Objective = (sum(cg*Pg1(1,:) + ...
%         p1*(c_rt*abs(Pg1_rt(1,:,1))+cs*Pshed(1,:,1)) +...
%         p2*(c_rt*abs(Pg1_rt(1,:,2))+cs*Pshed(1,:,2))));
% %         p3*(c_rt*abs(Pg1_rt(1,:,3))+cs*Pshed(1,:,3))));


add1 = 0;

for itt = 1:scenarios
    add1 = add1 + (p(itt).*( (c+delta)*y2(1,:,itt) - (c-eta)*y3(1,:,itt) ));
end

Objective = sum(-c*x(1,:) + add1);



% options = sdpsettings('verbose',1,'solver','ipopt');
options = sdpsettings('verbose',1,'solver','gurobi');
% options = sdpsettings('verbose',1,'solver','bmibnb');

%Solving the problem
sol = optimize(Constraints,Objective,options);


cal_time = sol.solvertime;

% it1 = it1+1;
%
% iterVal
%
% ObjIter(it1) = value(Objective);
%
objectiveFunctionValue = value(Objective);

% end


%% Results

% size(Objective)
% value(Objective)
% value(Pg1*BasePower)
% value(Pg2*BasePower)
% value(Pg1_rt*BasePower)
% value(Pshed*BasePower)
% value(Pspill*BasePower)
%
%
% figure
% plot(1:24, value(Pg1*BasePower))
% hold on
% plot(1:24, value(Pg2*BasePower))
% hold on
% plot(1:24, value(Pg1_rt*BasePower))
% hold on
% plot(1:24, value(Pshed*BasePower))
% hold on
% plot(1:24, value(Pspill*BasePower))
% xlim([1 23])
%
% figure
% plot(1:24, WindGenerationRealised)
% hold on
% plot(1:24, Pg2_max*ones(1,24))
% xlim([1 24])


%%
% figure
% plot(1:length(ObjIter),ObjIter)
% xlim([2 length(ObjIter)])

end