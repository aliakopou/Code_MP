clear all
% close all

%%

% mu = 50;
% sigma = 5;
% sz = [1,100000];

% x = 0:0.01:1;
% rng(12)
% samples_nonNormalized = getScenariosFromCDFfunction(2,10000,mu/10^6); % wind power values sampled from a normal distribution 
% 
% samples = samples_nonNormalized*0.01; % normalized values
% samples = betarnd(2,2);

% figure
% h = histogram(samples,20);
% h.Normalization = 'probability';

%%

% x = linspace(0,1,10000);
% 
% b=2000;
% 
% for i = 1:length(x)
%     y(i) = (b*(-x(i)*(x(i)-1))^(b-1)) / (x(i)^b + (1-x(i))^b)^2;
% end
% 
% probs = y/(length(x)-1);
% 
% samples = x*100;
% 
% figure
% scatter(samples, ones(1,length(samples)))

%% Parameter Definition 

b=200;

%-- Parameter names same as in DTU document 'Stochastic Linear Programming'
c = 10;
delta = 5;
eta = 2;
%--

pi_plus = c-eta; % cost for positive imbalance
pi_minus = c+delta; % cost for negative imbalance

%% Obtaining samples using 'Inverse Transform Sampling' method (https://en.wikipedia.org/wiki/Inverse_transform_sampling)

centerValueOfDistribution = 50; % if we were to consider the distribution as a gaussian this would be the mean
% centerValueOfDistribution = 130*10^6; % if we were to consider the distribution as a gaussian this would be the mean 

rng(12)
vector1 = rand(1,100000);

for i = 1:length(vector1)
    x_fromInverse(i) = 1/(nthroot((1/vector1(i) - 1),b)+1); %obtain normalized samples (wind power values) using the analytical expression of the inverse CDF function
end

samples = x_fromInverse*(centerValueOfDistribution*2); 

%Plot the range of values
figure 
scatter(samples, ones(1,length(samples)))


%%
q = linspace(0,1.6*centerValueOfDistribution,100000);  % contracted power value (values between 0 and 1)
% q = linspace(70*10^6,180*10^6,10000);  % contracted power value (values between 0 and 1)


count1 = 1;
count2 = 1;

for i = 1:length(q)
    sumForEachQ = 0;
    for j = 1:length(samples)
        temp1 =  samples(j) - q(i);
        if temp1 > 0
            funct = pi_plus*temp1;
        else
            funct = pi_minus*(-temp1);
        end
        sumForEachQ = sumForEachQ+funct;
    end
    
        sumVector(i) = sumForEachQ*1;
%     sumVector(i) = sumForEachQ*h.Values(count2);
%     %     sumVector(i) = sumForEachQ*1;
%     sumVector(i) = sumForEachQ*h.Values(count2);
end

figure
plot(q,sumVector/length(samples))
% xlim([1 99])

%find index min is achieved

[min1,ind1] = min(sumVector);

val1 = q(ind1);

% minAchieved = samples(52)

%% Newsvendor model critical fractile formula 

% test1 = norminv(pi_plus/(pi_plus+pi_minus),mu,sigma);


y = pi_plus/(pi_plus+pi_minus);

x_fromInverse = 1/(nthroot((1/y - 1),b)+1)*(centerValueOfDistribution*2);

% x_fromInverse = (y-sqrt(y-y^2))/(2*y-1);

% test1 = betainv(pi_plus/(pi_plus+pi_minus),mu,sigma)*0.01

%% Results

strcat('Numerically we find the optimal power value to be commited DA equal to ', {' '}, string(val1)) 
strcat('And using the critical fractile formula this value is equal to ', {' '}, string(x_fromInverse))

