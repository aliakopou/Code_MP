clear all
close all

%%  We work with normal distribution
mu = 50;
sigma = 5;
sz = [1,10000];

% x = 0:0.01:1;
rng(12)
samples_nonNormalized = normrnd(mu,sigma,sz); % wind power values sampled from a normal distribution 

max1 = max(samples_nonNormalized);

samples = samples_nonNormalized;

% samples = samples_nonNormalized/max1; % so that values are between 0 and 1

% samples = samples_nonNormalized*0.01; % so that values are between 0 and 1

%Plot the range of values
figure
scatter(samples,ones(1,length(samples_nonNormalized)));

% samples = betarnd(2,2);

% figure
% h = histogram(samples,20);
% h.Normalization = 'probability';

%%
q = linspace(20,80,10000);  % contracted power value (values between 0 and 1)

pi_plus = 1; % cost for positive imbalance
pi_minus = 2; % cost for negative imbalance

count1 = 1;
count2 = 1;

for i = 1:length(q)
    sumForEachQ = 0;
    for j = 1:length(samples)
        temp1 =  samples(j) - q(i);
        if temp1 > 0
            funct = pi_plus*temp1;
        else
            funct = pi_minus*(-temp1);
        end
        sumForEachQ = sumForEachQ+funct;
    end
    
        sumVector(i) = sumForEachQ*1;
%     sumVector(i) = sumForEachQ*h.Values(count2);
    count1 = count1+1;
    if mod(count1,50) == 0
        count2 = count2 + 1;
    end
%     %     sumVector(i) = sumForEachQ*1;
%     sumVector(i) = sumForEachQ*h.Values(count2);
end

figure
plot(q,sumVector/length(samples))
% xlim([1 99])

%find index min is achieved

[min1,ind1] = min(sumVector);

val1 = q(ind1);

% minAchieved = samples(52)

%% Newsvendor model critical fractile formula 

test1 = norminv(pi_plus/(pi_plus+pi_minus),mu,sigma);

% test1 = betainv(pi_plus/(pi_plus+pi_minus),mu,sigma)*0.01

%% Results

strcat('Numerically we find the optimal power value to be commited DA equal to ', {' '}, string(val1)) 
strcat('And using the critical fractile formula this value is equal to ', {' '}, string(test1))

