clear all
close all
clear variables
yalmip('clear')

%% Parameters

numOfScenarios = 100;

%%

for itVal=2:1:2
    
    [scens,probs] = getScenariosFromCDFfunction(itVal, numOfScenarios);
    
    [objectiveFunctionValue, cal_time,x] = NewsvendorAsSLP_ProductionSide(scens,probs);
    
    
end