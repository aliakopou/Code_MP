clear all
close all
clear variables
yalmip('clear')

%%

% discretization = 1/numberOfScens;  %discretize based on the number of scenarios
% range = 0:discretization:1;
% x= range; % set grid space

%-- Parameter names same as in DTU document 'Stochastic Linear Programming'
c = 10; 
delta = 5;
eta = 2;
%--

x = (c-eta)/(2*c + delta - eta);
% x = (eta)/(delta + eta);

b = 2; % payment value   

x_point = 130*1*10^6;
y_value = 0.5;


% figure

% for b = 3
% for b = [1,2,5,2000]
% for b = 1:1:100

a = -log((1/y_value - 1)^(1/b)+1)/log(x_point);


y =  (nthroot((1/x)-1,b) +1 )^(-1/a);

% fplot(y,[0 1])
% hold on

% end

% xlim([0 1])
% ylim([0 1])